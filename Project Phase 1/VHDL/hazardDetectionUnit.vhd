library ieee;

use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.constants.all;

entity hazardDetectionUnit is
	port(
		-- IN
		clk              : in  std_logic;
		ID_EX_RegisterRt : in  std_logic_vector(4 downto 0);
		IF_ID_RegisterRs : in  std_logic_vector(4 downto 0);
		IF_ID_RegisterRt : in  std_logic_vector(4 downto 0);
		ID_EX_MemRead    : in  std_logic;
		-- Out
		IF_ID_IFIDWRITE  : out std_logic;
		IF_ID_PCWRITE    : out std_logic
	);
end hazardDetectionUnit;

architecture behavioral of hazardDetectionUnit is
begin
	Hazard : process(clk)
	begin
		if (ID_EX_MemRead = '1') and ((ID_EX_RegisterRt = IF_ID_RegisterRs) or (ID_EX_RegisterRt = IF_ID_RegisterRt)) then
			IF_ID_PCWRITE   <= '1';
			IF_ID_IFIDWRITE <= '1';
		else
			IF_ID_PCWRITE   <= '0';
			IF_ID_IFIDWRITE <= '0';
		end if;
	end process Hazard;
end behavioral;