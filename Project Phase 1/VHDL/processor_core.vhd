library ieee;

use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.constants.all;
entity processor_core is
	port(
		clk      : in  std_logic;
		rst      : in  std_logic;
		run      : in  std_logic;

		-- memory read address
		instaddr : out std_logic_vector(31 downto 0);
		-- instruction 
		inst     : in  std_logic_vector(31 downto 0);
		-- signal to enable memory write		
		memwen   : out std_logic;

		-- memory  address
		memaddr  : out std_logic_vector(31 downto 0);
		-- memory write data
		memdw    : out std_logic_vector(31 downto 0);
		-- memory read
		memdr    : in  std_logic_vector(31 downto 0);
		-- indicate finish
		fin      : out std_logic;
		-- PC value when finish
		PCout    : out std_logic_vector(31 downto 0);
		regaddr  : in  std_logic_vector(4 downto 0);
		regdout  : out std_logic_vector(31 downto 0)
	);
end processor_core;

architecture arch_processor_core of processor_core is
	-- component definitions
	component regtable
		port(
			clk     : in  std_logic;
			rst     : in  std_logic;
			raddrA  : in  std_logic_vector(4 downto 0);
			raddrB  : in  std_logic_vector(4 downto 0);
			wen     : in  std_logic;
			waddr   : in  std_logic_vector(4 downto 0);
			din     : in  std_logic_vector(31 downto 0);
			doutA   : out std_logic_vector(31 downto 0);
			doutB   : out std_logic_vector(31 downto 0);
			extaddr : in  std_logic_vector(4 downto 0);
			extdout : out std_logic_vector(31 downto 0)
		);
	end component;

	component alu
		port(
			operation : in  std_logic_vector(3 downto 0);
			input_a   : in  std_logic_vector(31 downto 0);
			input_b   : in  std_logic_vector(31 downto 0);
			output    : out std_logic_vector(31 downto 0);
			zero      : out std_logic
		);
	end component;

	component control
		port(
			Opcode             : in  std_logic_vector(5 downto 0);
			Func               : in  std_logic_vector(5 downto 0);
			RegDst             : out std_logic;
			Jump               : out std_logic;
			Branch             : out std_logic;
			MemRead            : out std_logic;
			MemtoReg           : out std_logic;
			ALUOp              : out std_logic_vector(3 downto 0);
			MemWrite           : out std_logic;
			ALUSrc             : out std_logic;
			RegWrite           : out std_logic;
			IllegalInstruction : out std_logic
		);
	end component;

	component hazardDetectionUnit
		port(
			-- IN
			clk              : in  std_logic;
			ID_EX_RegisterRt : in  std_logic_vector(4 downto 0);
			IF_ID_RegisterRs : in  std_logic_vector(4 downto 0);
			IF_ID_RegisterRt : in  std_logic_vector(4 downto 0);
			ID_EX_MemRead    : in  std_logic;
			-- Out
			IF_ID_IFIDWRITE  : out std_logic;
			IF_ID_PCWRITE    : out std_logic
		);
	end component;
	component ForwardingUnit
		port(
			clk                    : in  std_logic;

			-- IN
			ID_EX_RegisterRs       : in  std_logic_vector(4 downto 0);
			ID_EX_RegisterRt       : in  std_logic_vector(4 downto 0);
			EX_MEM_RegisterRd      : in  std_logic_vector(4 downto 0);
			MEM_WB_RegisterRd      : in  std_logic_vector(4 downto 0);

			EX_MEM_REGWRITE        : in  std_logic;
			MEM_WB_REGWRITE        : in  std_logic;
			-- Out
			MuxA                   : out std_logic_vector(1 downto 0);
			MuxB                   : out std_logic_vector(1 downto 0);

			Last_REGWRITE          : std_logic;
			Last_RegisterWriteAddr : std_logic_vector(4 downto 0) := (others => '0')
		);
	end component;
	component sign_extend
		port(
			in_data  : in  std_logic_vector(15 downto 0);
			out_data : out std_logic_vector(31 downto 0)
		);
	end component;

	-- signals address related

	signal NextAddr : std_logic_vector(31 downto 0);
	signal JumpAddr : std_logic_vector(31 downto 0);

	-- Register
	signal RegisterRs : std_logic_vector(4 downto 0) := (others => '0');
	signal RegisterRt : std_logic_vector(4 downto 0) := (others => '0');
	signal RegisterRd : std_logic_vector(4 downto 0) := (others => '0');

	-- Control signals
	signal RegDst   : std_logic;
	signal Branch   : std_logic;
	signal MemRead  : std_logic;
	signal MemWrite : std_logic;
	signal ALUOp    : std_logic_vector(3 downto 0);
	signal MemtoReg : std_logic;
	signal ALUSrc   : std_logic;
	signal RegWrite : std_logic;
	signal Jump     : std_logic;

	-- ALU
	signal ALUZero   : std_logic;
	signal ALUResult : std_logic_vector(31 downto 0);
	signal ALUInputA : std_logic_vector(31 downto 0);
	signal ALUInputB : std_logic_vector(31 downto 0);

	-- Sign extend
	signal SignExtendResult : std_logic_vector(31 downto 0);

	-- etc

	signal PC                 : std_logic_vector(31 downto 0);
	signal IllegalInstruction : std_logic := '0';
	signal Terminated         : std_logic := '0';

	-- PIPELINE REGISTERS
	-- input : XX__YY__ZZ_D  , output : XX_YY__ZZ_Q
	-- IF_EX_INSTR_15_0_D means the sign-extended- form of bits 0 to bits 15

	-- IF_ID 
	-- IN
	signal IF_ID_INSTR_31_0_D  : std_logic_vector(31 downto 0); -- insert inst OK
	signal IF_ID_PCNEXT_31_0_D : std_logic_vector(31 downto 0); -- insert PC+4 OK
	signal IF_ID_PCWRITE       : std_logic; -- not yet add
	signal IF_ID_IFIDWRITE_D   : std_logic; -- not yet add
	signal IF_ID_IFIDWRITE_Q   : std_logic;

	signal IF_ID_JUMP_D        : std_logic; -- insert Jump in the Control part
	-- OUT
	signal IF_ID_INSTR_31_0_Q  : std_logic_vector(31 downto 0); -- D to Q add
	signal IF_ID_PCNEXT_31_0_Q : std_logic_vector(31 downto 0); -- D to Q add

	-- ID_EX
	-- IN
	signal ID_EX_ADDR_D    : std_logic_vector(31 downto 0);
	signal ID_EX_FUNCT_D   : std_logic_vector(5 downto 0);
	signal ID_EX_SIGNEXT_D : std_logic_vector(31 downto 0);
	signal ID_EX_RS_D      : std_logic_vector(4 downto 0);
	signal ID_EX_RT_D      : std_logic_vector(4 downto 0);
	signal ID_EX_RD_D      : std_logic_vector(4 downto 0);
	signal ID_EX_RegOut1_D : std_logic_vector(31 downto 0);
	signal ID_EX_RegOut2_D : std_logic_vector(31 downto 0);
	-- OUT
	signal ID_EX_ADDR_Q    : std_logic_vector(31 downto 0);
	signal ID_EX_FUNCT_Q   : std_logic_vector(5 downto 0);
	signal ID_EX_SIGNEXT_Q : std_logic_vector(31 downto 0);
	signal ID_EX_RS_Q      : std_logic_vector(4 downto 0);
	signal ID_EX_RT_Q      : std_logic_vector(4 downto 0);
	signal ID_EX_RD_Q      : std_logic_vector(4 downto 0);
	signal ID_EX_RegOut1_Q : std_logic_vector(31 downto 0);
	signal ID_EX_RegOut2_Q : std_logic_vector(31 downto 0);

	--	signal RegDst   : out std_logic ;
	--	signal Jump     : out std_logic ;
	signal ID_EX_Branch_D   : std_logic;
	signal ID_EX_Branch_Q   : std_logic;
	signal ID_EX_MemRead_D  : std_logic;
	signal ID_EX_MemRead_Q  : std_logic;
	signal ID_EX_MemtoReg_D : std_logic;
	signal ID_EX_MemtoReg_Q : std_logic;
	signal ID_EX_ALUOp_D    : std_logic_vector(3 downto 0);
	signal ID_EX_ALUOp_Q    : std_logic_vector(3 downto 0);
	signal ID_EX_MemWrite_D : std_logic;
	signal ID_EX_MemWrite_Q : std_logic;
	signal ID_EX_ALUSrc_D   : std_logic;
	signal ID_EX_ALUSrc_Q   : std_logic;
	signal ID_EX_RegWrite_D : std_logic;
	signal ID_EX_RegWrite_Q : std_logic;

	-- EX_MEM
	signal EX_MEM_MEMTOREG_D : std_logic;
	signal EX_MEM_MEMTOREG_Q : std_logic;

	signal EX_MEM_REGWRITE_D : std_logic;
	signal EX_MEM_REGWRITE_Q : std_logic;

	signal EX_MEM_MEMREAD_D  : std_logic;
	signal EX_MEM_MEMREAD_Q  : std_logic;
	signal EX_MEM_MEMWRITE_D : std_logic;
	signal EX_MEM_MEMWRITE_Q : std_logic;

	signal EX_MEM_ALURESULT_D : STD_LOGIC_VECTOR(31 downto 0);
	signal EX_MEM_ALURESULT_Q : STD_LOGIC_VECTOR(31 downto 0);

	signal EX_MEM_WRITEDATA_D : STD_LOGIC_VECTOR(31 downto 0);
	signal EX_MEM_WRITEDATA_Q : STD_LOGIC_VECTOR(31 downto 0);

	signal EX_MEM_REGISTERRD_D : STD_LOGIC_VECTOR(4 downto 0);
	signal EX_MEM_REGISTERRD_Q : STD_LOGIC_VECTOR(4 downto 0);

	-- MEM_WB

	signal MEM_WB_REGWRITE_D : std_logic;
	signal MEM_WB_REGWRITE_Q : std_logic;

	signal MEM_WB_MEMTOREG_D : std_logic;
	signal MEM_WB_MEMTOREG_Q : std_logic;

	signal MEM_WB_MEMDATA_D : STD_LOGIC_VECTOR(31 downto 0);
	signal MEM_WB_MEMDATA_Q : STD_LOGIC_VECTOR(31 downto 0);

	signal MEM_WB_ALURESULT_D : STD_LOGIC_VECTOR(31 downto 0);
	signal MEM_WB_ALURESULT_Q : STD_LOGIC_VECTOR(31 downto 0);

	signal MEM_WB_REGISTERRD_D : STD_LOGIC_VECTOR(4 downto 0);
	signal MEM_WB_REGISTERRD_Q : STD_LOGIC_VECTOR(4 downto 0);

	-- WB STAGE
	signal WB_RegisterWriteAddr : std_logic_vector(4 downto 0) := (others => '0');

	-- data write to the register
	signal WB_RegisterWriteData : std_logic_vector(31 downto 0) := (others => '0');
--	signal WB_REGWRITE          : std_logic;
	signal ForwardingMUXa       : std_logic_vector(1 downto 0);
	signal ForwardingMUXb       : std_logic_vector(1 downto 0);

	signal Falling_Rs : std_logic_vector(4 downto 0);
	signal Falling_Rt : std_logic_vector(4 downto 0);

	signal Rising_REGWRITE          : std_logic;
	signal Rising_RegisterWriteAddr : std_logic_vector(4 downto 0)  := (others => '0');
	-- data write to the register
	signal Rising_RegisterWriteData : std_logic_vector(31 downto 0) := (others => '0');

	signal StallCountDown : std_logic_vector(3 downto 0) := (others => '0');
	signal PCEnd          : std_logic_vector(31 downto 0) := (others => '0');
begin

	-- Processor Core Behaviour

	-- IF STAGE

	-- Adder: PC + 4 => Next Addr
	NextAddr <= std_logic_vector(ieee.NUMERIC_STD.unsigned(PC) + 4);

	-- Insert PC+4 to IF/ID
	IF_ID_PCNEXT_31_0_D <= NextAddr;
	-- insert inst to pipeline
	IF_ID_INSTR_31_0_D  <= inst;

	-- ID STAGE
	-- Register

	RegisterRs <= IF_ID_INSTR_31_0_Q(25 downto 21);
	RegisterRt <= IF_ID_INSTR_31_0_Q(20 downto 16);
	RegisterRd <= IF_ID_INSTR_31_0_Q(15 downto 11);

	RegisterTable : regtable port map(
			clk     => clk,
			rst     => rst,
			-- ID STAGE
			--			raddrA  => RegisterRs,
			--			raddrB  => RegisterRt,
			raddrA  => Falling_Rs,
			raddrB  => Falling_Rt,
			doutA   => ID_EX_RegOut1_D,
			doutB   => ID_EX_RegOut2_D,

			-- WB STAGE
			wen     => MEM_WB_REGWRITE_Q, --WB_REGWRITE,
			--		waddr   => Rising_RegisterWriteAddr,
			--		din     => Rising_RegisterWriteData,

			waddr   => WB_RegisterWriteAddr,
			din     => WB_RegisterWriteData,

			-- debug use signals
			extaddr => regaddr,
			extdout => regdout);
	--	WB_REGWRITE <= MEM_WB_REGWRITE_Q when clk = '1' else '0';
	-- Control

	Control32 : control port map(
			Opcode             => IF_ID_INSTR_31_0_Q(31 downto 26),
			Func               => IF_ID_INSTR_31_0_Q(5 downto 0),
			RegDst             => RegDst,
			--		Jump               => IF_ID_JUMP_D,
			--		Branch             => ID_EX_Branch_D,
			--		MemRead            => ID_EX_MemRead_D,
			--		MemtoReg           => ID_EX_MemtoReg_D,
			--		ALUOp              => ID_EX_ALUOp_D,
			--		MemWrite           => ID_EX_MemWrite_D,
			--		ALUSrc             => ID_EX_ALUSrc_D,
			--		RegWrite           => ID_EX_RegWrite_D,

			Jump               => Jump,
			Branch             => Branch,
			MemRead            => MemRead,
			MemtoReg           => MemtoReg,
			ALUOp              => ALUOp,
			MemWrite           => MemWrite,
			ALUSrc             => ALUSrc,
			RegWrite           => RegWrite,
			-- if illegal instruction is enocuntered => 1 => Halt!
			IllegalInstruction => IllegalInstruction
		);

	-- NextAddr take from IF stage while JumpAddr from ID Stage
	JumpAddr <= NextAddr(31 downto 28) & IF_ID_INSTR_31_0_Q(25 downto 0) & "00";

	-- insert the control into buffers
	IF_ID_JUMP_D <= Jump;

	ID_EX_FUNCT_D <= IF_ID_INSTR_31_0_Q(5 downto 0);
	ID_EX_RS_D    <= RegisterRs;
	ID_EX_RT_D    <= RegisterRt;
	ID_EX_RD_D    <= RegisterRd when (RegDst = '1') else RegisterRt;

	SignExtender : sign_extend port map(
			in_data  => IF_ID_INSTR_31_0_Q(15 downto 0),
			out_data => SignExtendResult
		);

	ID_EX_SIGNEXT_D <= SignExtendResult;

	-- branch address
	ID_EX_ADDR_D <= std_logic_vector(ieee.NUMERIC_STD.unsigned(IF_ID_PCNEXT_31_0_Q) + (ieee.NUMERIC_STD.unsigned(SignExtendResult) sll 2));

	HDU : hazardDetectionUnit port map(
			-- IN
			clk              => clk,
			ID_EX_RegisterRt => ID_EX_RT_Q,
			IF_ID_RegisterRs => IF_ID_INSTR_31_0_Q(25 downto 21),
			IF_ID_RegisterRt => IF_ID_INSTR_31_0_Q(20 downto 16),
			ID_EX_MemRead    => ID_EX_MemRead_Q,
			-- Out
			IF_ID_IFIDWRITE  => IF_ID_IFIDWRITE_D,
			IF_ID_PCWRITE    => IF_ID_PCWRITE
		);

	-- TODO: HDU want to stall one cycle of instruction
	-- i.e The next Instruction ID->EX
	-- Force control values in ID/EX register to 0
	ID_EX_Branch_D   <= '0' when (IF_ID_IFIDWRITE_D = '1') else Branch;
	ID_EX_MemRead_D  <= '0' when (IF_ID_IFIDWRITE_D = '1') else MemRead;
	ID_EX_MemtoReg_D <= '0' when (IF_ID_IFIDWRITE_D = '1') else MemtoReg;
	ID_EX_ALUOp_D    <= "0000" when (IF_ID_IFIDWRITE_D = '1') else ALUOp;
	ID_EX_MemWrite_D <= '0' when (IF_ID_IFIDWRITE_D = '1') else MemWrite;
	ID_EX_ALUSrc_D   <= '0' when (IF_ID_IFIDWRITE_D = '1') else ALUSrc;
	ID_EX_RegWrite_D <= '0' when (IF_ID_IFIDWRITE_D = '1') else RegWrite;
	-- Prevent update of PC and IF/ID register
	-- Using instruction is decoded again
	-- Following instruction is fetched again
	-- 1-cycle stall allows MEM to read data for lw
	-- Can subsequently forward to EX stage
	-- EX STAGE

	EX_MEM_MemRead_D  <= ID_EX_MemRead_Q;
	EX_MEM_MemtoReg_D <= ID_EX_MemtoReg_Q;
	EX_MEM_MemWrite_D <= ID_EX_MemWrite_Q;
	EX_MEM_RegWrite_D <= ID_EX_RegWrite_Q;

	-- TODO: forwarding unit MUXs

	ALUInputA <= Rising_RegisterWriteData when ForwardingMUXa = "11" else ID_EX_RegOut1_Q when ForwardingMUXa = "00" else -- 00 = FROM previous stage register output
		MEM_WB_MEMDATA_Q when (MEM_WB_MEMTOREG_Q = '1' and ForwardingMUXa = "01") else MEM_WB_ALURESULT_Q when ForwardingMUXa = "01" else -- from MEM STAGE
		EX_MEM_ALURESULT_Q when ForwardingMUXa = "10"; -- from EX STAGE

	-- MUX : ALUSrc 0 : from register output B , 1 , sign extended value

	ALUInputB <= Rising_RegisterWriteData when ForwardingMUXb = "11" else ID_EX_SIGNEXT_Q when (ID_EX_ALUSrc_Q = '1') else -- select by ALUSrc
		ID_EX_RegOut2_Q when ForwardingMUXb = "00" else -- 00 = FROM previous stage register output
		MEM_WB_MEMDATA_Q when (MEM_WB_MEMTOREG_Q = '1' and ForwardingMUXb = "01") else MEM_WB_ALURESULT_Q when ForwardingMUXb = "01" else -- from MEM STAGE
		EX_MEM_ALURESULT_Q when ForwardingMUXb = "10"; -- from EX STAGE

	--	ALUInputB <= ID_EX_RegOut2_Q when (ID_EX_ALUSrc_Q = '0') else ID_EX_SIGNEXT_Q;
	-- ALU
	ALU32 : alu port map(
			operation => ID_EX_ALUOp_Q,
			input_a   => ALUInputA,
			input_b   => ALUInputB,
			output    => ALUResult,
			zero      => ALUZero
		);

	EX_MEM_ALURESULT_D <= ALUResult;
	-- TODO: use forwarding unit mux result
	EX_MEM_WRITEDATA_D <= ID_EX_RegOut2_Q;

	EX_MEM_MEMREAD_D  <= ID_EX_MEMREAD_Q;
	EX_MEM_MEMTOREG_D <= ID_EX_MemtoReg_Q;
	EX_MEM_MEMWRITE_D <= ID_EX_MemWrite_Q;

	EX_MEM_REGISTERRD_D <= ID_EX_RD_Q;

	EX_MEM_REGWRITE_D <= ID_EX_RegWrite_Q;

	-- MEM STAGE
	-- signal memwen should be MemWrite


	MEM_WB_ALURESULT_D  <= EX_MEM_ALURESULT_Q;
	MEM_WB_MEMDATA_D    <= memdr;
	MEM_WB_REGWRITE_D   <= EX_MEM_REGWRITE_Q;
	MEM_WB_MEMTOREG_D   <= EX_MEM_MemtoReg_Q;
	MEM_WB_REGISTERRD_D <= EX_MEM_REGISTERRD_Q;

	-- WB STAGE
	WB_RegisterWriteAddr <= MEM_WB_REGISTERRD_Q;
	-- Mux : 1 : from Memory , 0 : from ALU
	WB_RegisterWriteData <= MEM_WB_MEMDATA_Q when (MEM_WB_MEMTOREG_Q = '1') else MEM_WB_ALURESULT_Q;

	memwen  <= EX_MEM_MEMWRITE_Q;
	-- write data to memory is output b of register
	memdw   <= EX_MEM_WRITEDATA_Q;
	-- memory address is the ALU result
	memaddr <= EX_MEM_ALURESULT_Q;
	FUnit : ForwardingUnit port map(
			clk                    => clk,

			-- IN
			ID_EX_RegisterRs       => ID_EX_RS_Q,
			ID_EX_RegisterRt       => ID_EX_RT_Q,
			--	ID_EX_RegisterRs  => ID_EX_RS_Q,
			--	ID_EX_RegisterRt  => ID_EX_RT_Q,
			EX_MEM_RegisterRd      => EX_MEM_REGISTERRD_Q,
			MEM_WB_RegisterRd      => MEM_WB_REGISTERRD_Q,
			EX_MEM_REGWRITE        => EX_MEM_REGWRITE_Q,
			MEM_WB_REGWRITE        => MEM_WB_REGWRITE_Q,
			-- Out
			MuxA                   => ForwardingMUXa,
			MuxB                   => ForwardingMUXb,

			-- Forwarding unit for hazard due to RegisterTable
			Last_REGWRITE          => Rising_REGWRITE,
			Last_RegisterWriteAddr => Rising_RegisterWriteAddr
		);

	ReadRegister : process(clk)
	begin
		if falling_edge(clk) then
			Falling_Rs <= RegisterRs;
			Falling_Rt <= RegisterRt;

		end if;
	end process ReadRegister;

	ProgramCounter : process(clk, rst)
		variable running : boolean := false;
		variable PCTemp  : std_logic_vector(31 downto 0);

	begin
		if rst = '1' then
			PC         <= x"00004000";
			Terminated <= '0';
			running    := false;
		--		PCOut    <= PC;

		--	report "[DEBUG] RESET";
		elsif clk = '1' and run = '1' then
			PC                 <= x"00004000";
			instaddr           <= x"00004000";
			Terminated         <= '0';
			IllegalInstruction <= '0';
			running            := true;
		elsif running and clk = '1' then
			if clk = '1' and rising_edge(clk) then
				-- WB STAGE
				Rising_REGWRITE          <= MEM_WB_REGWRITE_Q;
				Rising_RegisterWriteAddr <= MEM_WB_REGISTERRD_Q;
				-- Mux : 1 : from Memory , 0 : from ALU
				IF (MEM_WB_MEMTOREG_Q = '1') then
					Rising_RegisterWriteData <= MEM_WB_MEMDATA_Q;

				else
					Rising_RegisterWriteData <= MEM_WB_ALURESULT_Q;
				end if;

			--		elsif clk = '1' and falling_edge(clk) then
			--			Falling_Rs <= RegisterRs;
			--			Falling_Rt <= RegisterRt;
			end if;

			-- we do pipeline moving when rising clock
			--	if clk = '1' and rising_edge(clk) then
			-- PIPELINES

			-- IF stage
			--	if falling_edge(clk) then
			IF not (IF_ID_IFIDWRITE_D = '1') then
				IF_ID_INSTR_31_0_Q  <= IF_ID_INSTR_31_0_D;
				IF_ID_PCNEXT_31_0_Q <= IF_ID_PCNEXT_31_0_D;
				IF_ID_IFIDWRITE_Q   <= IF_ID_IFIDWRITE_D;
			end if;

			-- ID stage
			ID_EX_ADDR_Q    <= ID_EX_ADDR_D;
			ID_EX_FUNCT_Q   <= ID_EX_FUNCT_D;
			ID_EX_SIGNEXT_Q <= ID_EX_SIGNEXT_D;
			ID_EX_RS_Q      <= ID_EX_RS_D;
			ID_EX_RT_Q      <= ID_EX_RT_D;
			ID_EX_RD_Q      <= ID_EX_RD_D;

			--	if (Rising_REGWRITE = '1' and ID_EX_RS_D = Rising_RegisterWriteAddr) then
			--		ID_EX_RegOut1_Q <= Rising_RegisterWriteData;
			--	else
			ID_EX_RegOut1_Q <= ID_EX_RegOut1_D;
			--	end if;
			--	if (Rising_REGWRITE = '1' and ID_EX_RT_D = Rising_RegisterWriteAddr) then
			--		ID_EX_RegOut2_Q <= Rising_RegisterWriteData;
			--	else
			ID_EX_RegOut2_Q <= ID_EX_RegOut2_D;
			--	end if;

			ID_EX_Branch_Q      <= ID_EX_Branch_D;
			ID_EX_MemRead_Q     <= ID_EX_MemRead_D;
			ID_EX_MemtoReg_Q    <= ID_EX_MemtoReg_D;
			ID_EX_ALUOp_Q       <= ID_EX_ALUOp_D;
			ID_EX_MemWrite_Q    <= ID_EX_MemWrite_D;
			ID_EX_ALUSrc_Q      <= ID_EX_ALUSrc_D;
			ID_EX_RegWrite_Q    <= ID_EX_RegWrite_D;
			-- EX_MEM
			EX_MEM_MEMTOREG_Q   <= EX_MEM_MEMTOREG_D;
			EX_MEM_REGWRITE_Q   <= EX_MEM_REGWRITE_D;
			EX_MEM_MEMREAD_Q    <= EX_MEM_MEMREAD_D;
			EX_MEM_MEMWRITE_Q   <= EX_MEM_MEMWRITE_D;
			EX_MEM_ALURESULT_Q  <= EX_MEM_ALURESULT_D;
			EX_MEM_WRITEDATA_Q  <= EX_MEM_WRITEDATA_D;
			EX_MEM_REGISTERRD_Q <= EX_MEM_REGISTERRD_D;

			-- MEM_WB
			MEM_WB_REGWRITE_Q   <= MEM_WB_REGWRITE_D;
			MEM_WB_MEMTOREG_Q   <= MEM_WB_MEMTOREG_D;
			MEM_WB_MEMDATA_Q    <= MEM_WB_MEMDATA_D;
			MEM_WB_ALURESULT_Q  <= MEM_WB_ALURESULT_D;
			MEM_WB_REGISTERRD_Q <= MEM_WB_REGISTERRD_D;
			--		end if;
			if clk = '1' and rising_edge(clk) then
				-- Exception checking
				if (not (Jump = '1') and (Terminated = '1' or (inst = x"00000000") or (IllegalInstruction = '1') or (
							(ID_EX_MemRead_Q = '1' or ID_EX_MemWrite_Q = '1') and (
								not (conv_integer(ALUResult) mod 4 = 0)
							)))) or (
					(Jump = '1') and not (conv_integer(JumpAddr) mod 4 = 0)
				) then
					StallCountDown <= "0101";
					PCEnd          <= PC;
				end if;
			end if;
			-- PC selection
			if (IF_ID_PCWRITE = '1') then
				-- stall pipeline here
				PC       <= PC;
				instaddr <= PC;

			else
				PCTemp := NextAddr;
				if (ID_EX_Branch_Q = '1' and ALUZero = '1') then
					report "[DEBUG] CLOCK RISING - Branch";
					PCTemp := ID_EX_ADDR_Q; -- value from before register moving

					-- IF Flush
					IF_ID_INSTR_31_0_Q  <= x"00000000";
					IF_ID_PCNEXT_31_0_Q <= x"00000000";

					-- ID Flush
					ID_EX_ADDR_Q    <= x"00000000";
					ID_EX_FUNCT_Q   <= "000000";
					ID_EX_SIGNEXT_Q <= x"00000000";
					ID_EX_RS_Q      <= "00000";
					ID_EX_RT_Q      <= "00000";
					ID_EX_RD_Q      <= "00000";
					ID_EX_RegOut1_Q <= x"00000000";
					ID_EX_RegOut2_Q <= x"00000000";

					ID_EX_Branch_Q   <= '0';
					ID_EX_MemRead_Q  <= '0';
					ID_EX_MemtoReg_Q <= '0';
					ID_EX_ALUOp_Q    <= "0000";
					ID_EX_MemWrite_Q <= '0';
					ID_EX_ALUSrc_Q   <= '0';
					ID_EX_RegWrite_Q <= '0';
				end if;

				if (IF_ID_JUMP_D = '1') then
					report "[DEBUG] CLOCK RISING - Jump";
					--		Jump                <= '0';
					--		IF_ID_JUMP_D        <= '0';
					PCTemp              := JumpAddr;
					IF_ID_INSTR_31_0_Q  <= x"00000000";
					IF_ID_PCNEXT_31_0_Q <= x"00000000";

				end if;

				if (not (PCEnd = x"00000000")) then
					if (StallCountDown = "0000") then
						running := false;
						fin     <= '1';
						PCOut   <= PCEnd;
					end if;
					
					StallCountDown <=std_logic_vector(ieee.NUMERIC_STD.signed(StallCountDown) - 1);
				else
					PC       <= PCTemp;
					instaddr <= PCTemp;
				end if;
			end if;

		end if;
	end process ProgramCounter;
end arch_processor_core; 

