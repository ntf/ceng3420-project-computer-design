1.Group Member information
Mok Long Yat 1155029363
Ng Ting Fung 1155030851

2.

alu.vhd 
	The ALU component 
	input A & B , ALUOp , output result and zero output

constants.vhd 
	Package contains all common use constant
	OpCode , ALUOp Code , Function in R-TYPE instruction

control.vhd
    input are opcode and funct bits from instruction
	output are control signal and one extra for detect illegal instruction
processor_core.vhd
	the main implementation of the processor
	includes Program counter , use other components
sign_extend.vhd
	take 16 bits input and sign extends to 32 bits as output
	
regtable.vhd, memtable.vhd , processor.vhd , processor_tb.vhd : provided vhdl file

