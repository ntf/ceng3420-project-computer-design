library ieee;

use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.constants.all;
entity ForwardingUnit is
	port(
		clk                    : in  std_logic;

		-- IN
		ID_EX_RegisterRs       : in  std_logic_vector(4 downto 0);
		ID_EX_RegisterRt       : in  std_logic_vector(4 downto 0);
		EX_MEM_RegisterRd      : in  std_logic_vector(4 downto 0);
		MEM_WB_RegisterRd      : in  std_logic_vector(4 downto 0);

		EX_MEM_REGWRITE        : in  std_logic;
		MEM_WB_REGWRITE        : in  std_logic;
		-- Out
		MuxA                   : out std_logic_vector(1 downto 0);
		MuxB                   : out std_logic_vector(1 downto 0);

		Last_REGWRITE          : in  std_logic;
		Last_RegisterWriteAddr : in  std_logic_vector(4 downto 0) := (others => '0')
	);
end ForwardingUnit;

architecture behavioral of ForwardingUnit is
begin
	MuxA <= "11" when ((Last_REGWRITE = '1') and (
				not (Last_RegisterWriteAddr = "00000")
			) and (Last_RegisterWriteAddr = ID_EX_RegisterRs)) else "10" when ((EX_MEM_REGWRITE = '1') and (
				not (EX_MEM_RegisterRd = "00000")
			) and (EX_MEM_RegisterRd = ID_EX_RegisterRs)) else "01" when ((MEM_WB_REGWRITE = '1') and (not (MEM_WB_RegisterRd = "00000")) and (not (EX_MEM_RegisterRd = ID_EX_RegisterRs)) and (MEM_WB_RegisterRd = ID_EX_RegisterRs))
		else "00";

	MuxB <= "11" when ((Last_REGWRITE = '1') and (
				not (Last_RegisterWriteAddr = "00000")
			) and (Last_RegisterWriteAddr = ID_EX_RegisterRt)) else "10" when ((EX_MEM_REGWRITE = '1') and (not (EX_MEM_RegisterRd = "00000")) and (EX_MEM_RegisterRd = ID_EX_RegisterRt)) else "01" when ((MEM_WB_REGWRITE = '1') and (not (MEM_WB_RegisterRd = "00000")) and (not (
					EX_MEM_RegisterRd = ID_EX_RegisterRt)) and (
				MEM_WB_RegisterRd = ID_EX_RegisterRt)) else "00";

end behavioral;