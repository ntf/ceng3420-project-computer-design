-------------------------------------------------------------------------------
-- This is the top level of the single cycle MIPS processor.
--
-- Chris Sagedy
-- ECEC 490: Processor Design
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ConstantsPkg.all;
use work.ComponentsPkg.all;

entity Processor is
  generic (
    InstructionMemContents : T_MemoryArray := (others => (others => '0'));
    DataMemContents        : T_MemoryArray := (others => (others => '0'))
    );
  port (
    in_Clock : in std_logic;
    in_Reset : in std_logic
    );
end Processor;

architecture structural of Processor is

  -- Processor
  signal PC : std_logic_vector(31 downto 0) := (others => '0');

  -- Instruction Memory
  signal Instruction : std_logic_vector(31 downto 0);

  -- Instruction Decoder
  signal Opcode   : std_logic_vector(5 downto 0);
  signal RS       : std_logic_vector(4 downto 0);
  signal RT       : std_logic_vector(4 downto 0);
  signal RD       : std_logic_vector(4 downto 0);
  signal Shamt    : std_logic_vector(4 downto 0);
  signal Funct    : std_logic_vector(5 downto 0);
  signal IAddress : std_logic_vector(15 downto 0);
  signal JAddress : std_logic_vector(25 downto 0);

  -- Controller
  signal RegDst   : std_logic;
  signal Branch   : std_logic;
  signal MemRead  : std_logic;
  signal MemWrite : std_logic;
  signal ALUOp    : std_logic_vector(3 downto 0);
  signal MemtoReg : std_logic;
  signal ALUSrc   : std_logic;
  signal RegWrite : std_logic;
  signal Jump     : std_logic;

  -- Register File
  signal WriteReg      : std_logic_vector(4 downto 0);
  signal RegData1      : std_logic_vector(31 downto 0);
  signal RegData2      : std_logic_vector(31 downto 0);
  signal WriteBackData : std_logic_vector(31 downto 0);

  -- Sign Extender
  signal ExtendedAddress : std_logic_vector(31 downto 0);

  -- ALU
  signal InputA    : std_logic_vector(31 downto 0);
  signal InputB    : std_logic_vector(31 downto 0);
  signal ALUResult : std_logic_vector(31 downto 0);
  signal ZeroFlag  : std_logic;

  -- Data Memory
  signal MemoryData : std_logic_vector(31 downto 0);

  -- Program Counter
  signal NextAddress    : std_logic_vector(31 downto 0);
  signal BranchAddress  : std_logic_vector(31 downto 0);
  signal JumpAddress    : std_logic_vector(31 downto 0);
  signal CorrectAddress : std_logic_vector(31 downto 0);
  
begin

  -----------------------------------------------------------------------------
  -- Instantiate the instruction memory.
  -----------------------------------------------------------------------------
  InstructionMemory : Memory
    generic map (
      DefaultContents => InstructionMemContents
      )
    port map (
      in_Clock   => in_Clock,
      in_Reset   => in_Reset,
      in_Address => PC,
      in_Data    => (others => '0'),
      in_WriteEn => '0',
      in_ReadEn  => '1',
      out_Data   => Instruction
      );

  -----------------------------------------------------------------------------
  -- Instantiate the instruction decoder.
  -----------------------------------------------------------------------------
  InstructionDecode : InstructionDecoder
    port map (
      in_Instruction => Instruction,
      out_Opcode     => Opcode,
      out_RS         => RS,
      out_RT         => RT,
      out_RD         => RD,
      out_Shamt      => Shamt,
      out_Funct      => Funct,
      out_IAddress   => IAddress,
      out_JAddress   => JAddress
      );

  -----------------------------------------------------------------------------
  -- Instantiate the main control block.
  -----------------------------------------------------------------------------
  Control : Controller
    port map (
      in_Opcode    => Opcode,
      in_Funct     => Funct,
      out_RegDst   => RegDst,
      out_Branch   => Branch,
      out_MemRead  => MemRead,
      out_MemWrite => MemWrite,
      out_ALUOp    => ALUOp,
      out_MemtoReg => MemtoReg,
      out_ALUSrc   => ALUSrc,
      out_RegWrite => RegWrite,
      out_Jump     => Jump
      );

  -----------------------------------------------------------------------------
  -- Instantiate the register file.
  -----------------------------------------------------------------------------
  WriteReg      <= RT        when (RegDst = '0')   else RD;
  WriteBackData <= ALUResult when (MemtoReg = '0') else MemoryData;

  RegFile : RegisterFile
    port map (
      in_Clock    => in_Clock,
      in_Reset    => in_Reset,
      in_ReadReg1 => RS,
      in_ReadReg2 => RT,
      in_WriteReg => WriteReg,
      in_Data     => WriteBackData,
      in_WriteEn  => RegWrite,
      out_Data1   => RegData1,
      out_Data2   => RegData2
      );

  -----------------------------------------------------------------------------
  -- Instantiate the sign extender.
  -----------------------------------------------------------------------------
  SignExtend : SignExtender
    generic map (
      InputWidth  => 16,
      OutputWidth => 32
      )
    port map (
      in_Data  => IAddress,
      out_Data => ExtendedAddress
      );

  -----------------------------------------------------------------------------
  -- Instantiate the ALU.
  -----------------------------------------------------------------------------
  InputA <= RegData1;
  InputB <= RegData2 when (ALUSrc = '0') else ExtendedAddress;

  ALU : ALU32
    port map (
      in_Operation => ALUOp,
      in_A         => InputA,
      in_B         => InputB,
      out_Result   => ALUResult,
      out_Zero     => ZeroFlag
      );

  -----------------------------------------------------------------------------
  -- Instantiate data memory.
  -----------------------------------------------------------------------------
  DataMemory : Memory
    generic map (
      DefaultContents => DataMemContents
      )
    port map (
      in_Clock   => in_Clock,
      in_Reset   => in_Reset,
      in_Address => ALUResult,
      in_Data    => RegData2,
      in_WriteEn => MemWrite,
      in_ReadEn  => MemRead,
      out_Data   => MemoryData
      );

  -----------------------------------------------------------------------------
  -- Update the program counter.
  -----------------------------------------------------------------------------
  NextAddress   <= std_logic_vector(unsigned(PC) + 4);
  BranchAddress <= std_logic_vector(unsigned(NextAddress) + unsigned(ExtendedAddress));
  JumpAddress   <= NextAddress(31 downto 28) & JAddress & "00";

  UpdatePC : process (in_Clock, in_Reset)
  begin
    
    if in_Reset = '1' then
      PC <= (others => '0');
      
    elsif rising_edge(in_Clock) then
      if Jump = '1' then
        PC <= JumpAddress;
      elsif Branch = '1' and ZeroFlag = '1' then
        PC <= BranchAddress;
      else
        PC <= NextAddress;
      end if;

    end if;
    
  end process UpdatePC;
  
end structural;
