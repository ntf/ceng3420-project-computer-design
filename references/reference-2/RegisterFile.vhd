-------------------------------------------------------------------------------
-- This is a register file for the single cycle MIPS processor.
--
-- Chris Sagedy
-- ECEC 490: Processor Design
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity RegisterFile is
  port (
    in_Clock    : in  std_logic;
    in_Reset    : in  std_logic;
    in_ReadReg1 : in  std_logic_vector(4 downto 0);
    in_ReadReg2 : in  std_logic_vector(4 downto 0);
    in_WriteReg : in  std_logic_vector(4 downto 0);
    in_Data     : in  std_logic_vector(31 downto 0);
    in_WriteEn  : in  std_logic;
    out_Data1   : out std_logic_vector(31 downto 0);
    out_Data2   : out std_logic_vector(31 downto 0)
    );
end RegisterFile;

architecture behavioral of RegisterFile is

  type T_Register_Array is array(0 to 31) of std_logic_vector (31 downto 0);
  signal Registers : T_Register_Array;
  
begin
  
  out_Data1 <= Registers(to_integer(unsigned(in_ReadReg1)));
  out_Data2 <= Registers(to_integer(unsigned(in_ReadReg2)));

  WriteRegister : process (in_Clock, in_Reset)
  begin
    if in_Reset = '1' then
      Registers <= (others => x"00000000");
    elsif rising_edge(in_Clock) then
      if (in_WriteEn = '1') then
        Registers(to_integer(unsigned(in_WriteReg))) <= in_Data;
      end if;
    end if;
  end process WriteRegister;
  
end behavioral;
