-------------------------------------------------------------------------------
-- This package contains all the component declarations for the single cycle
-- MIPS processor.
--
-- Chris Sagedy
-- ECEC 490: Processor Design
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use work.ConstantsPkg.all;

package ComponentsPkg is

  component Processor
    generic (
      InstructionMemContents : T_MemoryArray := (others => (others => '0'));
      DataMemContents        : T_MemoryArray := (others => (others => '0'))
      );
    port (
      in_Clock : in std_logic;
      in_Reset : in std_logic
      );
  end component;

  component InstructionDecoder
    port (
      in_Instruction : in  std_logic_vector(31 downto 0);
      out_Opcode     : out std_logic_vector(5 downto 0);
      out_RS         : out std_logic_vector(4 downto 0);
      out_RT         : out std_logic_vector(4 downto 0);
      out_RD         : out std_logic_vector(4 downto 0);
      out_Shamt      : out std_logic_vector(4 downto 0);
      out_Funct      : out std_logic_vector(5 downto 0);
      out_IAddress   : out std_logic_vector(15 downto 0);
      out_JAddress   : out std_logic_vector(25 downto 0)
      );
  end component;

  component Controller
    port (
      in_Opcode    : in  std_logic_vector(5 downto 0);
      in_Funct     : in  std_logic_vector(5 downto 0);
      out_RegDst   : out std_logic;
      out_Branch   : out std_logic;
      out_MemRead  : out std_logic;
      out_MemWrite : out std_logic;
      out_ALUOp    : out std_logic_vector(3 downto 0);
      out_MemtoReg : out std_logic;
      out_ALUSrc   : out std_logic;
      out_RegWrite : out std_logic;
      out_Jump     : out std_logic
      );
  end component;

  component Memory
    generic (
      DefaultContents : T_MemoryArray := (others => (others => '0'))
      );
    port (
      in_Clock   : in  std_logic;
      in_Reset   : in  std_logic;
      in_Address : in  std_logic_vector (31 downto 0);
      in_Data    : in  std_logic_vector (31 downto 0);
      in_WriteEn : in  std_logic;
      in_ReadEn  : in  std_logic;
      out_Data   : out std_logic_vector (31 downto 0)
      );
  end component;

  component RegisterFile
    port (
      in_Clock    : in  std_logic;
      in_Reset    : in  std_logic;
      in_ReadReg1 : in  std_logic_vector(4 downto 0);
      in_ReadReg2 : in  std_logic_vector(4 downto 0);
      in_WriteReg : in  std_logic_vector(4 downto 0);
      in_Data     : in  std_logic_vector(31 downto 0);
      in_WriteEn  : in  std_logic;
      out_Data1   : out std_logic_vector(31 downto 0);
      out_Data2   : out std_logic_vector(31 downto 0)
      );
  end component;

  component SignExtender
    generic (
      InputWidth  : integer := 16;
      OutputWidth : integer := 32
      );
    port (
      in_Data  : in  std_logic_vector(InputWidth-1 downto 0);
      out_Data : out std_logic_vector(OutputWidth-1 downto 0)
      );
  end component;

  component ALU32
    port (
      in_Operation : in  std_logic_vector(3 downto 0);
      in_A         : in  std_logic_vector(31 downto 0);
      in_B         : in  std_logic_vector(31 downto 0);
      out_Result   : out std_logic_vector(31 downto 0);
      out_Zero     : out std_logic
      );
  end component;

end ComponentsPkg;

package body ComponentsPkg is



end ComponentsPkg;
