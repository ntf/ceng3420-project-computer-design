-------------------------------------------------------------------------------
-- This is a generic memory module and is used as both the instruction and data
-- memory in the single cycle MIPS processor.
--
-- Chris Sagedy
-- ECEC 490: Processor Design
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ConstantsPkg.all;

entity Memory is
  generic (
    DefaultContents : T_MemoryArray := (others => (others => '0'))
    );
  port (
    in_Clock   : in  std_logic;
    in_Reset   : in  std_logic;
    in_Address : in  std_logic_vector(31 downto 0);
    in_Data    : in  std_logic_vector(31 downto 0);
    in_WriteEn : in  std_logic;
    in_ReadEn  : in  std_logic;
    out_Data   : out std_logic_vector(31 downto 0)
    );
end Memory;

architecture behavioral of Memory is
  
  signal Memory : T_MemoryArray := DefaultContents;
  
begin

  UpdateOutputs : process (in_Address, in_ReadEn)
  begin
    if in_ReadEn = '1' then
      out_Data(31 downto 24) <= Memory(to_integer(unsigned(in_Address) + 3));
      out_Data(23 downto 16) <= Memory(to_integer(unsigned(in_Address) + 2));
      out_Data(15 downto 8)  <= Memory(to_integer(unsigned(in_Address) + 1));
      out_Data(7 downto 0)   <= Memory(to_integer(unsigned(in_Address))); 
    else
      out_Data <= (others => '0');
    end if;
    
  end process UpdateOutputs;


  WriteMemory : process (in_Clock, in_Reset)
  begin
    if in_Reset = '1' then
      Memory <= DefaultContents;
    elsif rising_edge(in_Clock) then
      if (in_WriteEn = '1') then
        Memory(to_integer(unsigned(in_Address) + 3)) <= in_Data(31 downto 24);
        Memory(to_integer(unsigned(in_Address) + 2)) <= in_Data(23 downto 16);
        Memory(to_integer(unsigned(in_Address) + 1)) <= in_Data(15 downto 8);
        Memory(to_integer(unsigned(in_Address)))     <= in_Data(7 downto 0);
      end if;
    end if;
  end process WriteMemory;
  
end behavioral;
