library IEEE;
use IEEE.std_logic_1164.all;

package constants is
	-- Opcodes
	constant OP_R_TYPE : std_logic_vector(5 downto 0) := "000000";
	constant OP_ADDI   : std_logic_vector(5 downto 0) := "001000";
	constant OP_BEQ    : std_logic_vector(5 downto 0) := "000100";
	constant OP_LUI    : std_logic_vector(5 downto 0) := "001111";
	constant OP_LW    : std_logic_vector(5 downto 0) := "100011";
	constant OP_SW    : std_logic_vector(5 downto 0) := "101011";
	constant OP_SLTI  : std_logic_vector(5 downto 0) := "001010";
	constant OP_SLTIU : std_logic_vector(5 downto 0) := "001011";
	constant OP_J     : std_logic_vector(5 downto 0) := "000010";

	-- ALU Op
	constant ALU_AND : std_logic_vector(3 downto 0) := "0000";
	constant ALU_OR  : std_logic_vector(3 downto 0) := "0001";
	constant ALU_ADD : std_logic_vector(3 downto 0) := "0010";
	constant ALU_SUB : std_logic_vector(3 downto 0) := "0110";
	constant ALU_SLT : std_logic_vector(3 downto 0) := "0111";
	constant ALU_NOR : std_logic_vector(3 downto 0) := "1100";
	constant ALU_SLL  : std_logic_vector(3 downto 0) := "1000";
	constant ALU_SLTU : std_logic_vector(3 downto 0) := "1001";
	constant ALU_LUI : std_logic_vector(3 downto 0) := "1010";
	constant ALU_NOP  : std_logic_vector(3 downto 0) := "1111";

	-- FUNC
	constant FUNC_ADD  : std_logic_vector(5 downto 0) := "100000";
	constant FUNC_ADDU : std_logic_vector(5 downto 0) := "100001";
	constant FUNC_AND  : std_logic_vector(5 downto 0) := "100100";
	constant FUNC_JR   : std_logic_vector(5 downto 0) := "001000";
	constant FUNC_NOR  : std_logic_vector(5 downto 0) := "100111";
	constant FUNC_OR   : std_logic_vector(5 downto 0) := "100101";
	constant FUNC_SLT  : std_logic_vector(5 downto 0) := "101010";
	constant FUNC_SLTU : std_logic_vector(5 downto 0) := "101011";
	constant FUNC_SLL  : std_logic_vector(5 downto 0) := "000000";
	constant FUNC_SRL  : std_logic_vector(5 downto 0) := "000010";
	constant FUNC_SUB  : std_logic_vector(5 downto 0) := "100010";
	constant FUNC_SUBU : std_logic_vector(5 downto 0) := "100011";

end constants;

package body constants is
end constants;
