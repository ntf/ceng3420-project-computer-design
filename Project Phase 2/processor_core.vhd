library ieee;

use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.constants.all;
entity processor_core is
	port(
		clk      : in  std_logic;
		rst      : in  std_logic;
		run      : in  std_logic;

		-- memory read address
		instaddr : out std_logic_vector(31 downto 0);
		-- instruction 
		inst     : in  std_logic_vector(31 downto 0);
		-- signal to enable memory write		
		memwen   : out std_logic;

		-- memory  address
		memaddr  : out std_logic_vector(31 downto 0);
		-- memory write data
		memdw    : out std_logic_vector(31 downto 0);
		-- memory read
		memdr    : in  std_logic_vector(31 downto 0);
		-- indicate finish
		fin      : out std_logic;
		-- PC value when finish
		PCout    : out std_logic_vector(31 downto 0);
		regaddr  : in  std_logic_vector(4 downto 0);
		regdout  : out std_logic_vector(31 downto 0)
	);
end processor_core;

architecture arch_processor_core of processor_core is
	-- component definitions
	component regtable
		port(
			clk     : in  std_logic;
			rst     : in  std_logic;
			raddrA  : in  std_logic_vector(4 downto 0);
			raddrB  : in  std_logic_vector(4 downto 0);
			wen     : in  std_logic;
			waddr   : in  std_logic_vector(4 downto 0);
			din     : in  std_logic_vector(31 downto 0);
			doutA   : out std_logic_vector(31 downto 0);
			doutB   : out std_logic_vector(31 downto 0);
			extaddr : in  std_logic_vector(4 downto 0);
			extdout : out std_logic_vector(31 downto 0)
		);
	end component;

	component alu
		port(
			operation : in  std_logic_vector(3 downto 0);
			input_a   : in  std_logic_vector(31 downto 0);
			input_b   : in  std_logic_vector(31 downto 0);
			output    : out std_logic_vector(31 downto 0);
			zero      : out std_logic
		);
	end component;

	component control
		port(
			Opcode             : in  std_logic_vector(5 downto 0);
			Func               : in  std_logic_vector(5 downto 0);
			RegDst             : out std_logic;
			Jump               : out std_logic;
			Branch             : out std_logic;
			MemRead            : out std_logic;
			MemtoReg           : out std_logic;
			ALUOp              : out std_logic_vector(3 downto 0);
			MemWrite           : out std_logic;
			ALUSrc             : out std_logic;
			RegWrite           : out std_logic;
			IllegalInstruction : out std_logic
		);
	end component;

	component sign_extend
		port(
			in_data  : in  std_logic_vector(15 downto 0);
			out_data : out std_logic_vector(31 downto 0)
		);
	end component;

	-- signals address related

	signal NextAddr   : std_logic_vector(31 downto 0);
	signal JumpAddr   : std_logic_vector(31 downto 0);
	signal BranchAddr : std_logic_vector(31 downto 0);

	-- Register
	signal RegisterRs : std_logic_vector(4 downto 0) := (others => '0');
	signal RegisterRt : std_logic_vector(4 downto 0) := (others => '0');

	signal RegisterRd : std_logic_vector(4 downto 0) := (others => '0');

	signal RegisterWriteAddr : std_logic_vector(4 downto 0) := (others => '0');

	-- data write to the register
	signal RegisterWriteData : std_logic_vector(31 downto 0) := (others => '0');

	-- data A and B read from register
	signal RegisterReadDataA : std_logic_vector(31 downto 0) := (others => '0');
	signal RegisterReadDataB : std_logic_vector(31 downto 0) := (others => '0');

	-- Control signals
	signal RegDst   : std_logic;
	signal Branch   : std_logic;
	signal MemRead  : std_logic;
	signal MemWrite : std_logic;
	signal ALUOp    : std_logic_vector(3 downto 0);
	signal MemtoReg : std_logic;
	signal ALUSrc   : std_logic;
	signal RegWrite : std_logic;
	signal Jump     : std_logic;

	-- ALU
	signal ALUZero   : std_logic;
	signal ALUResult : std_logic_vector(31 downto 0);
	signal ALUInputA : std_logic_vector(31 downto 0);
	signal ALUInputB : std_logic_vector(31 downto 0);

	-- Sign extend
	signal SignExtendResult : std_logic_vector(31 downto 0);

	-- etc
	signal BranchOrNext       : std_logic_vector(31 downto 0);
	signal PC                 : std_logic_vector(31 downto 0);
	signal IllegalInstruction : std_logic := '0';
	signal Terminated         : std_logic := '0';
begin

	-- Processor Core Behaviour

	-- Adder: PC + 4 => Next Addr
	NextAddr <= std_logic_vector(ieee.NUMERIC_STD.unsigned(PC) + 4);

	-- Shifter: PC+4(31:28) & (inst(25:0) shift left two bits) => 32BITS
	JumpAddr <= NextAddr(31 downto 28) & inst(25 downto 0) & "00";

	-- PC+4 (base) + Sign Extended shift left two bits (offset) => Branch Address;
	BranchAddr <= std_logic_vector(ieee.NUMERIC_STD.unsigned(NextAddr) + (ieee.NUMERIC_STD.unsigned(SignExtendResult) sll 2));

	-- Register
	RegisterRs <= inst(25 downto 21);
	RegisterRt <= inst(20 downto 16);
	RegisterRd <= inst(15 downto 11);

	-- Mux : 0 : Rt , 1 : Rd
	RegisterWriteAddr <= RegisterRd when (RegDst = '1') else RegisterRt;
	-- Mux : 1 : from Memory , 0 : from ALU
	RegisterWriteData <= memdr when (MemToReg = '1') else ALUResult;

	RegisterTable : regtable port map(
			clk     => clk,
			rst     => rst,
			raddrA  => RegisterRs,
			raddrB  => RegisterRt,
			wen     => RegWrite,
			waddr   => RegisterWriteAddr,
			din     => RegisterWriteData,
			doutA   => RegisterReadDataA,
			doutB   => RegisterReadDataB,
			-- debug use signals
			extaddr => regaddr,
			extdout => regdout);

	-- write data to memory is output b of register
	memdw   <= RegisterReadDataB;
	-- memory address is the ALU result
	memaddr <= ALUResult;

	-- ALU behaviour


	-- SignExtender behaviour
	SignExtender : sign_extend port map(
			in_data  => inst(15 downto 0),
			out_data => SignExtendResult
		);
	ALUInputA <= RegisterReadDataA;
	-- MUX : ALUSrc 0 : from register output B , 1 , sign extended value
	ALUInputB <= RegisterReadDataB when (ALUSrc = '0') else SignExtendResult;
	-- ALU
	ALU32 : alu port map(
			operation => ALUOp,
			input_a   => ALUInputA,
			input_b   => ALUInputB,
			output    => ALUResult,
			zero      => ALUZero
		);

	-- Control

	Control32 : control port map(
			Opcode             => inst(31 downto 26),
			Func               => inst(5 downto 0),
			RegDst             => RegDst,
			Jump               => Jump,
			Branch             => Branch,
			MemRead            => MemRead,
			MemtoReg           => MemtoReg,
			ALUOp              => ALUOp,
			MemWrite           => MemWrite,
			ALUSrc             => ALUSrc,
			RegWrite           => RegWrite,
			-- if illegal instruction is enocuntered => 1 => Halt!
			IllegalInstruction => IllegalInstruction
		);
	-- signal memwen should be MemWrite
	memwen <= MemWrite;

	Terminated <= '1' when (
			(Terminated = '1') or (IllegalInstruction = '1')
		) else '0';
	-- Section 2.7 Conditions that should halt
	Terminated <= '1' when (
			(Terminated = '1') or (inst = x"00000000")
		) else '0';
	Terminated <= '1' when (
			(Terminated = '1') or ((Jump = '1') and conv_integer(JumpAddr) mod 4 = 0)
		) else '0';
	Terminated <= '1' when (
			(Terminated = '1') or (
				(inst(31 downto 26) = OP_LW or inst(31 downto 26) = OP_SW) and not (conv_integer(ALUResult) mod 4 = 0)
			)
		) else '0';

	ProgramCounter : process(clk, rst)
		variable running : boolean := false;
		variable PCTemp  : std_logic_vector(31 downto 0);
		variable temp    : integer;
	begin
		if rst = '1' then
			PC         <= x"00004000";
			Terminated <= '0';
			running    := false;
			--		PCOut    <= PC;

		--	report "[DEBUG] RESET";
		elsif clk = '0' and run = '1' then
			PC         <= x"00004000";
			instaddr   <= x"00004000";
			Terminated <= '0';
			running    := true;
		end if;

		if running then
			if clk = '1' and clk'event and rising_edge(clk) then
				if (Branch = '1' and ALUZero = '1') then
					--	report "[DEBUG] CLOCK RISING - Branch";
					PCTemp := BranchAddr;
				elsif (Jump = '1') then
					--	report "[DEBUG] CLOCK RISING - Jump";
					PCTemp := JumpAddr;
				else
					--	report "[DEBUG] CLOCK RISING - Next";
					PCTemp := NextAddr;
				end if;
				--	temp := conv_integer(ALUResult);
				--	temp := conv_integer(ALUResult) mod 4;

				if Terminated = '1' or (inst = x"00000000") or (IllegalInstruction = '1') or (
					(inst(31 downto 26) = OP_LW or inst(31 downto 26) = OP_SW) and (
						not (conv_integer(ALUResult) mod 4 = 0)
					)) or (
					(Jump = '1') and not (conv_integer(JumpAddr) mod 4 = 0)
				) then
					running := false;
					fin     <= '1';
					PCOut   <= PC;
				end if;

				PC       <= PCTemp;
				instaddr <= PCTemp;
			end if;
		end if;
	end process ProgramCounter;
end arch_processor_core;

