library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

entity sign_extend is
	port (
		in_data : in std_logic_vector(15 downto 0);
		out_data : out std_logic_vector(31 downto 0)
	);
end sign_extend;

architecture behavioral of sign_extend is
begin

  -- simple process to sign extend 16 bit in data
  UpdateOutput: process (in_Data)
  begin
    out_Data(15 downto 0) <= in_Data(15 downto 0);
    
    -- 31-16 bits = #15 bit value
    out_Data(31 downto 16) <= (others => in_Data(15));
  end process UpdateOutput;

end behavioral;