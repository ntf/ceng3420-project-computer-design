library ieee;

use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.constants.all;
entity ForwardingUnit is
	port(
		clk             : in  std_logic;
		rst             : in  std_logic;
		run             : in  std_logic;

		-- IN
		IDEXRegisterRs  : in  std_logic_vector(4 downto 0);
		IDEXRegisterRt  : in  std_logic_vector(4 downto 0);
		EXMEMRegisterRd : in  std_logic_vector(4 downto 0);
		MEMWBRegisterRd : in  std_logic_vector(4 downto 0);

		-- Out
		MuxA            : out std_logic;
		MuxB            : out std_logic
	);
end ForwardingUnit;