library ieee;

use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.constants.all;
entity MEMStage is
	port(
		clk               : in  std_logic;
		rst               : in  std_logic;
		run               : in  std_logic;

		-- Control for other stage
		-- Control for WB
		inWBMemtoReg      : in  std_logic;
		inWBRegWrite      : in  std_logic;

		-- Control for M
		inMBranch         : in  std_logic;
		inMMemRead        : in  std_logic;
		inMMemWrite       : in  std_logic;

		-- From EXMEM buffer
		EXMEMALUresult    : in  std_logic_vector(31 downto 0);
		EXMEMWriteData    : in  std_logic_vector(31 downto 0);
		EXMEMRegisterRd   : in  std_logic_vector(4 downto 0);

		-- Out
		-- Control for WB
		outWBMemtoReg     : out std_logic;
		outWBRegWrite     : out std_logic;

		-- to MEMWB
		MEMWBMemData      : out std_logic_vector(31 downto 0);
		MEMWBALUResult    : out std_logic_vector(31 downto 0);
		MEMWBRegisterRd   : out std_logic_vector(4 downto 0);
		-- to Forwarding Unit
		FUEXMEMRegisterRd : out std_logic_vector(4 downto 0);

		-- To EX
		EXALUResult       : out std_logic_vector(31 downto 0)
	);
end MEMStage;