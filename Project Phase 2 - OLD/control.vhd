library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.constants.all;
-- control signals
entity control is
	port(
		Opcode             : in  std_logic_vector(5 downto 0);
		Func               : in  std_logic_vector(5 downto 0);
		RegDst             : out std_logic;
		Jump               : out std_logic;
		Branch             : out std_logic;
		MemRead            : out std_logic;
		MemtoReg           : out std_logic;
		ALUOp              : out std_logic_vector(3 downto 0);
		MemWrite           : out std_logic;
		ALUSrc             : out std_logic;
		RegWrite           : out std_logic;

		-- extra signal indicate illegal instruction
		IllegalInstruction : out std_logic
	);
end control;

architecture behavioral of control is
begin
	-- control
	GenerateSignal : process(Opcode, Func)
		variable Illegal : std_logic := '0';
	begin
		Illegal := '0';
		case Opcode is
			when OP_R_TYPE =>
				RegDST   <= '1';
				Branch   <= '0';
				MemRead  <= '0';
				MemWrite <= '0';
				MemtoReg <= '0';
				ALUSrc   <= '0';
				RegWrite <= '1';
				Jump     <= '0';
				-- set AluOp according to funct of the instruction 
				case Func is
					when FUNC_ADD =>
						ALUOp <= ALU_ADD;
					when FUNC_SUB =>
						ALUOp <= ALU_SUB;
					when FUNC_AND =>
						ALUOp <= ALU_AND;
					when FUNC_OR =>
						ALUOp <= ALU_OR;
					when FUNC_NOR =>
						ALUOp <= ALU_NOR;
					when FUNC_SLT =>
						ALUOp <= ALU_SLT;
					when FUNC_SLTU =>
						ALUOp <= ALU_SLTU;
					when others =>
						Illegal := '1';
						ALUOp   <= ALU_NOP;
				end case;

			when OP_ADDI =>
				RegDST   <= '0';
				Branch   <= '0';
				MemRead  <= '0';
				MemWrite <= '0';
				ALUOp    <= ALU_ADD;
				MemtoReg <= '0';
				-- select sign extended immediate 
				ALUSrc   <= '1';
				RegWrite <= '1';
				Jump     <= '0';

			-- load word
			when OP_LW =>
				RegDST   <= '0';
				Branch   <= '0';
				MemRead  <= '1';
				MemWrite <= '0';
				ALUOp    <= ALU_ADD;
				MemtoReg <= '1';
				ALUSrc   <= '1';
				RegWrite <= '1';
				Jump     <= '0';
			-- save word
			when OP_SW =>
				RegDST   <= '0';
				Branch   <= '0';
				MemRead  <= '0';
				MemWrite <= '1';
				ALUOp    <= ALU_ADD;
				MemtoReg <= '0';
				ALUSrc   <= '1';
				RegWrite <= '0';
				Jump     <= '0';
			-- load upper immediate
			when OP_LUI =>
				RegDST   <= '0';
				Branch   <= '0';
				MemRead  <= '0';
				MemWrite <= '0';
				ALUOp    <= ALU_LUI;
				MemtoReg <= '0';
				ALUSrc   <= '1';
				RegWrite <= '1';
				Jump     <= '0';
			-- BEQ
			when OP_BEQ =>
				RegDST   <= '0';
				-- branching if A - B == 0
				Branch   <= '1';
				MemRead  <= '0';
				MemWrite <= '0';
				ALUOp    <= ALU_SUB;
				MemtoReg <= '0';
				ALUSrc   <= '0';
				RegWrite <= '0';
				Jump     <= '0';
			-- slt
			when OP_SLTI =>
				RegDST   <= '0';
				Branch   <= '0';
				MemRead  <= '0';
				MemWrite <= '0';
				ALUOp    <= ALU_SLT;
				MemtoReg <= '0';
				ALUSrc   <= '1';
				RegWrite <= '1';
				Jump     <= '0';
			when OP_SLTIU =>
				RegDST   <= '0';
				Branch   <= '0';
				MemRead  <= '0';
				MemWrite <= '0';
				ALUOp    <= ALU_SLTU;
				MemtoReg <= '0';
				ALUSrc   <= '1';
				RegWrite <= '1';
				Jump     <= '0';

			-- JUMP
			when OP_J =>
				RegDST   <= '0';
				Branch   <= '0';
				MemRead  <= '0';
				MemWrite <= '0';
				ALUOp    <= ALU_NOP;
				MemtoReg <= '0';
				ALUSrc   <= '0';
				RegWrite <= '0';
				-- just jump
				Jump     <= '1';

			when others =>
				--illegal instruction
				Illegal  := '1';
				RegDST   <= '0';
				Branch   <= '0';
				MemRead  <= '0';
				MemWrite <= '0';
				ALUOp    <= ALU_NOP;
				MemtoReg <= '0';
				ALUSrc   <= '0';
				RegWrite <= '0';
				Jump     <= '0';
		end case;

		if Illegal = '1' then
			IllegalInstruction <= '1';
		else
			IllegalInstruction <= '0';
		end if;
	end process GenerateSignal;

end behavioral;
