library ieee;

use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.constants.all;
entity processor_core is
	port(
		clk      : in  std_logic;
		rst      : in  std_logic;
		run      : in  std_logic;

		-- memory read address
		instaddr : out std_logic_vector(31 downto 0);
		-- instruction 
		inst     : in  std_logic_vector(31 downto 0);
		-- signal to enable memory write		
		memwen   : out std_logic;

		-- memory  address
		memaddr  : out std_logic_vector(31 downto 0);
		-- memory write data
		memdw    : out std_logic_vector(31 downto 0);
		-- memory read
		memdr    : in  std_logic_vector(31 downto 0);
		-- indicate finish
		fin      : out std_logic;
		-- PC value when finish
		PCout    : out std_logic_vector(31 downto 0);
		regaddr  : in  std_logic_vector(4 downto 0);
		regdout  : out std_logic_vector(31 downto 0)
	);
end processor_core;

architecture arch_processor_core of processor_core is
	-- component definitions
	component regtable
		port(
			clk     : in  std_logic;
			rst     : in  std_logic;
			raddrA  : in  std_logic_vector(4 downto 0);
			raddrB  : in  std_logic_vector(4 downto 0);
			wen     : in  std_logic;
			waddr   : in  std_logic_vector(4 downto 0);
			din     : in  std_logic_vector(31 downto 0);
			doutA   : out std_logic_vector(31 downto 0);
			doutB   : out std_logic_vector(31 downto 0);
			extaddr : in  std_logic_vector(4 downto 0);
			extdout : out std_logic_vector(31 downto 0)
		);
	end component;

	component alu
		port(
			operation : in  std_logic_vector(3 downto 0);
			input_a   : in  std_logic_vector(31 downto 0);
			input_b   : in  std_logic_vector(31 downto 0);
			output    : out std_logic_vector(31 downto 0);
			zero      : out std_logic
		);
	end component;

	component control
		port(
			Opcode             : in  std_logic_vector(5 downto 0);
			Func               : in  std_logic_vector(5 downto 0);
			RegDst             : out std_logic;
			Jump               : out std_logic;
			Branch             : out std_logic;
			MemRead            : out std_logic;
			MemtoReg           : out std_logic;
			ALUOp              : out std_logic_vector(3 downto 0);
			MemWrite           : out std_logic;
			ALUSrc             : out std_logic;
			RegWrite           : out std_logic;
			IllegalInstruction : out std_logic
		);
	end component;

	component sign_extend
		port(
			in_data  : in  std_logic_vector(15 downto 0);
			out_data : out std_logic_vector(31 downto 0)
		);
	end component;

	-- components for pipline 
	-- IF stage
	component IFStage
		port(
			clk             : in  std_logic;
			rst             : in  std_logic;
			run             : in  std_logic;
			-- indicate finish
			fin             : out std_logic;
			-- PC value when finish
			PCout           : out std_logic_vector(31 downto 0);

			-- memory related
			-- memory read address
			instaddr        : out std_logic_vector(31 downto 0);
			-- instruction 
			inst            : in  std_logic_vector(31 downto 0);
			-- signal to enable memory write		
			memwen          : out std_logic;
			-- memory  address
			memaddr         : out std_logic_vector(31 downto 0);
			-- memory write data
			memdw           : out std_logic_vector(31 downto 0);

			--======================== Phase 2 code ========================--
			-- In
			Jump            : in  std_logic;
			JumpAddr        : in  std_logic_vector(31 downto 0);
			Branch          : in  std_logic;
			BranchAddr      : in  std_logic_vector(31 downto 0);
			PCWrite         : in  std_logic;

			-- Out to IF/ID
			IFIDinstruction : out std_logic_vector(31 downto 0);
			IFIDnextAddr    : out std_logic_vector(31 downto 0);

			-- Terminate
			ALUResult       : in  std_logic_vector(31 downto 0)
		);
	end component;
	-- ID stage
	component IDStage
		port(
			clk               : in  std_logic;
			rst               : in  std_logic;
			run               : in  std_logic;

			-- Data Input Signals
			IFIDnextAddr      : in  std_logic_vector(31 downto 0);
			IFIDinstruction   : in  std_logic_vector(31 downto 0);
			RegisterWriteAddr : in  std_logic_vector(4 downto 0)  := (others => '0');
			RegisterRegWrite  : in  std_logic;
			RegisterWriteData : in  std_logic_vector(31 downto 0) := (others => '0');
			--======================== Phase 1 code ========================--
			-- Control Output Signals
			RegDst            : out std_logic;
			Branch            : out std_logic;
			MemRead           : out std_logic;
			MemWrite          : out std_logic;
			ALUOp             : out std_logic_vector(3 downto 0);
			MemtoReg          : out std_logic;
			ALUSrc            : out std_logic;
			RegWrite          : out std_logic;
			Jump              : out std_logic;

			-- Register
			RegisterRs        : out std_logic_vector(4 downto 0)  := (others => '0');
			RegisterRt        : out std_logic_vector(4 downto 0)  := (others => '0');

			RegisterRd        : out std_logic_vector(4 downto 0)  := (others => '0');

			RegOutputA        : out std_logic_vector(31 downto 0);
			RegOutputB        : out std_logic_vector(31 downto 0);

			JumpAddr          : out std_logic_vector(31 downto 0);
			SignExtendResult  : out std_logic_vector(31 downto 0);

			--======================== Phase 2 code ========================--
			-- Out 
			-- Control for WB
			WBMemtoReg        : out std_logic;
			WBRegWrite        : out std_logic;

			-- Control for M
			MBranch           : out std_logic;
			MMemRead          : out std_logic;
			MMemWrite         : out std_logic;

			-- Control for EX
			EXRegDst          : out std_logic;
			EXALUOp           : out std_logic_vector(3 downto 0);
			EXALUSrc          : out std_logic;

			-- To IDEX buffer
			IDEXaddr          : out std_logic_vector(31 downto 0);
			IDEXfunct         : out std_logic_vector(5 downto 0);
			IDEXSignExtend    : out std_logic_vector(31 downto 0);
			IDEXRegisterRs    : out std_logic_vector(4 downto 0);
			IDEXRegisterRt    : out std_logic_vector(4 downto 0);
			IDEXRegisterRd    : out std_logic_vector(4 downto 0);
			IDEXRegOutputA    : out std_logic_vector(31 downto 0);
			IDEXRegOutputB    : out std_logic_vector(31 downto 0)
		);
	end component;
	-- EX stage
	component EXStage
		port(
			clk             : in  std_logic;
			rst             : in  std_logic;
			run             : in  std_logic;

			--======================== Phase 2 code ========================--
			-- IN
			-- From IDEX buffer
			IDEXaddr        : in  std_logic_vector(31 downto 0);
			IDEXfunct       : in  std_logic_vector(5 downto 0);
			IDEXRegOutputA  : in  std_logic_vector(31 downto 0);
			IDEXRegOutputB  : in  std_logic_vector(31 downto 0);
			IDEXSignExtend  : in  std_logic_vector(31 downto 0);
			IDEXRegisterRs  : in  std_logic_vector(4 downto 0);
			IDEXRegisterRt  : in  std_logic_vector(4 downto 0);
			IDEXRegisterRd  : in  std_logic_vector(4 downto 0);
			EXMEMALUresult  : in  std_logic_vector(31 downto 0);
			MEMWBResult     : in  std_logic_vector(31 downto 0);

			-- From Forwarding Unit
			MuxA            : in  std_logic;
			MuxB            : in  std_logic;
			-- Control for other stage
			-- Control for WB
			inWBMemtoReg    : in  std_logic;
			inWBRegWrite    : in  std_logic;

			-- Control for M
			inMBranch       : in  std_logic;
			inMMemRead      : in  std_logic;
			inMMemWrite     : in  std_logic;

			-- Control for EX
			inEXRegDst      : in  std_logic;
			inEXALUOp       : in  std_logic_vector(3 downto 0);
			inEXALUSrc      : in  std_logic;

			-- Out

			ALUZero         : out std_logic;

			-- Out to EXMEM
			-- Control out
			-- Control for WB
			outWBMemtoReg   : out std_logic;
			outWBRegWrite   : out std_logic;

			-- Control for M
			outMBranch      : out std_logic;
			outMMemRead     : out std_logic;
			outMMemWrite    : out std_logic;

			EXMEMALUResult  : out std_logic_vector(31 downto 0);
			EXMEMWriteData  : out std_logic_vector(31 downto 0);
			EXMEMRegisterRd : out std_logic_vector(4 downto 0));
	end component;
	-- MEM stage
	component MEMStage
		port(
			clk               : in  std_logic;
			rst               : in  std_logic;
			run               : in  std_logic;

			-- Control for other stage
			-- Control for WB
			inWBMemtoReg      : in  std_logic;
			inWBRegWrite      : in  std_logic;

			-- Control for M
			inMBranch         : in  std_logic;
			inMMemRead        : in  std_logic;
			inMMemWrite       : in  std_logic;

			-- From EXMEM buffer
			EXMEMALUresult    : in  std_logic_vector(31 downto 0);
			EXMEMWriteData    : in  std_logic_vector(31 downto 0);
			EXMEMRegisterRd   : in  std_logic_vector(4 downto 0);

			-- Out
			-- Control for WB
			outWBMemtoReg     : out std_logic;
			outWBRegWrite     : out std_logic;
			-- to MEMWB
			MEMWBMemData      : out std_logic_vector(31 downto 0);
			MEMWBALUResult    : out std_logic_vector(31 downto 0);
			MEMWBRegisterRd   : out std_logic_vector(4 downto 0);
			-- to Forwarding Unit
			FUEXMEMRegisterRd : out std_logic_vector(4 downto 0);

			-- To EX
			EXALUResult       : out std_logic_vector(31 downto 0)
		);
	end component;
	-- WB stage
	component WBStage
		port(
			clk               : in  std_logic;
			rst               : in  std_logic;
			run               : in  std_logic;

			-- In
			-- Control WB
			inWBMemtoReg      : in  std_logic;
			inWBRegWrite      : in  std_logic;

			-- From MEMWB
			MEMWBMemData      : in  std_logic_vector(31 downto 0);
			MEMWBALUresult    : in  std_logic_vector(31 downto 0);
			MEMWBRegisterRd   : in  std_logic_vector(4 downto 0);

			-- Out
			-- to Forwarding Unit
			FUMEMWBRegisterRd : out std_logic_vector(4 downto 0);
			FUMemWrite        : out std_logic;
			-- to EX
			EXWriteData       : out std_logic_vector(31 downto 0);
			-- to ID
			IDWriteData       : out std_logic_vector(31 downto 0);
			IDMemWrite        : out std_logic
		);
	end component;
	component ForwardingUnit
		port(
			clk             : in  std_logic;
			rst             : in  std_logic;
			run             : in  std_logic;

			-- IN
			IDEXRegisterRs  : in  std_logic_vector(4 downto 0);
			IDEXRegisterRt  : in  std_logic_vector(4 downto 0);
			EXMEMRegisterRd : in  std_logic_vector(4 downto 0);
			MEMWBRegisterRd : in  std_logic_vector(4 downto 0);

			-- Out
			MuxA            : out std_logic;
			MuxB            : out std_logic
		);

	end component;
	-- HazardDetectionUnit
	component HazardDetectionUnit
		port(
			clk                : in  std_logic;
			rst                : in  std_logic;
			run                : in  std_logic;

			-- In
			signal IDEXmemread : in  std_logic;
			IDEXRegisterRt     : in  std_logic_vector(4 downto 0);
			IFIDRegisterRt     : in  std_logic_vector(4 downto 0);
			-- Out
			signal IFIDWrite   : out std_logic;
			signal PCWrite     : out std_logic
		);
	end component;

	signal IF_Flush : std_logic;
	signal ID_Flush : std_logic;
	signal EX_Flush : std_logic;

	-- IF Stage
	signal IFIDNextAddr    : std_logic_vector(31 downto 0);
	signal IFIDInstruction : std_logic_vector(31 downto 0);
	signal IFIDRS          : std_logic_vector(4 downto 0);
	signal IFIDRT          : std_logic_vector(4 downto 0);

	-- ID Stage
	signal IDEXRegDst   : std_logic;
	signal IDEXALUOp    : std_logic_vector(3 downto 0);
	signal IDEXALUSrc   : std_logic;
	signal IDEXBranch   : std_logic;
	signal IDEXMemRead  : std_logic;
	signal IDEXMemWrite : std_logic;
	signal IDEXMemtoReg : std_logic;
	signal IDEXRegWrite : std_logic;
	signal IDEXJump     : std_logic;

	signal IDEXaddr       : std_logic_vector(31 downto 0);
	signal IDEXfunct      : std_logic_vector(5 downto 0);
	signal IDEXSignExtend : std_logic_vector(31 downto 0);
	signal IDEXRegisterRs : std_logic_vector(4 downto 0);
	signal IDEXRegisterRt : std_logic_vector(4 downto 0);
	signal IDEXRegisterRd : std_logic_vector(4 downto 0);
	signal IDEXRegOutputA : std_logic_vector(31 downto 0);
	signal IDEXRegOutputB : std_logic_vector(31 downto 0);

	-- EXMEM Stage

	signal EXMEMJump       : std_logic;
	signal EXMEMJumpAddr   : std_logic_vector(31 downto 0);
	signal EXMEMBranch     : std_logic;
	signal EXMEMBranchAddr : std_logic_vector(31 downto 0);
	signal EXMEMALUResult  : std_logic_vector(31 downto 0);

	signal MEMWBResult : std_logic_vector(31 downto 0);

	-- WB Stage
	signal WBWriteEnable     : std_logic;
	signal WBWriteReg        : std_logic_vector(4 downto 0);
	signal WBWriteData       : std_logic_vector(31 downto 0);
	signal FORWARD_MUXA      : std_logic;
	signal FORWARD_MUXB      : std_logic;
	signal EXMEMWriteData    : std_logic_vector(31 downto 0);
	signal EXMEMRegisterRd   : std_logic_vector(4 downto 0);
	signal MEMWBMemData      : std_logic_vector(31 downto 0);
	signal MEMWBALUResult    : std_logic_vector(31 downto 0);
	signal MEMWBRegisterRd   : std_logic_vector(4 downto 0);
	signal FUEXMEMRegisterRd : std_logic_vector(4 downto 0);
	signal inWBMemtoReg      : std_logic;
	signal inWBRegWrite      : std_logic;
	signal EXALUResult       : std_logic_vector(31 downto 0);
	signal inMBranch         : std_logic;
	signal inMMemRead        : std_logic;
	signal inMMemWrite       : std_logic;
	signal outWBMemtoReg     : std_logic;
	signal outWBRegWrite     : std_logic;

	-- control signals
	signal EXRegDst          : std_logic;
	signal EXALUOp           : std_logic_vector(3 downto 0);
	signal EXALUSrc          : std_logic;
	signal MBranch           : std_logic;
	signal MMemRead          : std_logic;
	signal MMemWrite         : std_logic;
	signal WBMemtoReg        : std_logic;
	signal WBRegWrite        : std_logic;
	signal FUMEMWBRegisterRd : std_logic_vector(4 downto 0);
	signal FUMemWrite        : std_logic;
	signal EXWriteData       : std_logic_vector(31 downto 0);
	signal IDWriteData       : std_logic_vector(31 downto 0);
	signal IDMemWrite        : std_logic;
	signal IFIDRegisterRt    : std_logic_vector(4 downto 0);
	signal IFIDWrite         : std_logic;
	signal PCWrite           : std_logic;

	signal FORWARD_PCWrite : std_logic;

begin

	-- Processor Core Behaviour

	-- Add IF stage
	Stage_IF : IFStage port map(
			clk             => clk,
			rst             => rst,
			run             => run,
			fin             => fin,
			PCout           => PCout,
			instaddr        => instaddr,
			-- instruction 
			inst            => inst,
			-- signal to enable memory write		
			memwen          => memwen,
			-- memory  address
			memaddr         => memaddr,
			-- memory write data
			memdw           => memdw,
			Jump            => EXMEMJump,
			JumpAddr        => EXMEMJumpAddr,
			Branch          => EXMEMBranch,
			BranchAddr      => EXMEMBranchAddr,
			PCWrite         => FORWARD_PCWrite,

			-- Out to IF/ID
			IFIDinstruction => IFIDInstruction,
			IFIDnextAddr    => IFIDNextAddr,
			-- Terminate (input)
			ALUResult       => EXMEMALUResult
		);

	-- Add ID stage
	Stage_ID : IDStage port map(
			clk               => clk,
			rst               => rst,
			run               => run,
			-- IN
			IFIDnextAddr      => IFIDNextAddr,
			IFIDinstruction   => IFIDInstruction,
			RegisterRegWrite  => WBWriteEnable,
			RegisterWriteAddr => WBWriteReg,
			RegisterWriteData => WBWriteData,

			-- TO IDEX buffer
			IDEXaddr          => IDEXaddr,
			IDEXfunct         => IDEXfunct,
			IDEXSignExtend    => IDEXSignExtend,
			IDEXRegisterRs    => IDEXRegisterRs,
			IDEXRegisterRt    => IDEXRegisterRt,
			IDEXRegisterRd    => IDEXRegisterRd,
			IDEXRegOutputA    => IDEXRegOutputA,
			IDEXRegOutputB    => IDEXRegOutputB,

			-- control signals
			EXRegDst          => EXRegDst,
			EXALUOp           => EXALUOp,
			EXALUSrc          => EXALUSrc,
			MBranch           => MBranch,
			MMemRead          => MMemRead,
			MMemWrite         => MMemWrite,
			WBMemtoReg        => WBMemtoReg,
			WBRegWrite        => WBRegWrite
		);
	Stage_EX : EXStage port map(
			clk             => clk,
			rst             => rst,
			run             => run,
			IDEXaddr        => IDEXaddr,
			IDEXfunct       => IDEXfunct,
			IDEXRegOutputA  => IDEXRegOutputA,
			IDEXRegOutputB  => IDEXRegOutputB,
			IDEXSignExtend  => IDEXSignExtend,
			IDEXRegisterRs  => IDEXRegisterRs,
			IDEXRegisterRt  => IDEXRegisterRt,
			IDEXRegisterRd  => IDEXRegisterRd,
			EXMEMALUresult  => EXMEMALUresult,
			EXMEMWriteData  => EXMEMWriteData,
			EXMEMRegisterRd => EXMEMRegisterRd,
			MEMWBResult     => MEMWBResult,

			-- control signals
			inEXRegDst      => EXRegDst,
			inEXALUOp       => EXALUOp,
			inEXALUSrc      => EXALUSrc,
			inMBranch       => MBranch,
			inMMemRead      => MMemRead,
			inMMemWrite     => MMemWrite,
			inWBMemtoReg    => WBMemtoReg,
			inWBRegWrite    => WBRegWrite,
			outMBranch      => MBranch,
			outMMemRead     => MMemRead,
			outMMemWrite    => MMemWrite,
			outWBMemtoReg   => WBMemtoReg,
			outWBRegWrite   => WBRegWrite,
			-- forward
			MuxA            => FORWARD_MUXA,
			MuxB            => FORWARD_MUXB
		);
	Stage_MEM : MEMStage port map(
			clk               => clk,
			rst               => rst,
			run               => run,

			-- Control for WB
			inWBMemtoReg      => inWBMemtoReg,
			inWBRegWrite      => inWBRegWrite,

			-- Control for M
			inMBranch         => inMBranch,
			inMMemRead        => inMMemRead,
			inMMemWrite       => inMMemWrite,

			-- From EXMEM buffer
			EXMEMALUresult    => EXMEMALUresult,
			EXMEMWriteData    => EXMEMWriteData,
			EXMEMRegisterRd   => EXMEMRegisterRd,

			-- Out
			-- Control for WB
			outWBMemtoReg     => outWBMemtoReg,
			outWBRegWrite     => outWBRegWrite,
			-- to MEMWB
			MEMWBMemData      => MEMWBMemData,
			MEMWBALUResult    => MEMWBALUResult,
			MEMWBRegisterRd   => MEMWBRegisterRd,
			-- to Forwarding Unit
			FUEXMEMRegisterRd => FUEXMEMRegisterRd,

			-- To EX
			EXALUResult       => EXALUResult
		);

	Stage_WB : WBStage port map(
			clk               => clk,
			rst               => rst,
			run               => run,

			-- In
			-- Control WB
			inWBMemtoReg      => inWBMemtoReg,
			inWBRegWrite      => inWBMemtoReg,
			-- From MEMWB
			MEMWBMemData      => MEMWBMemData,
			MEMWBALUresult    => MEMWBALUresult,
			MEMWBRegisterRd   => MEMWBRegisterRd,

			-- Out
			-- to Forwarding Unit
			FUMEMWBRegisterRd => FUMEMWBRegisterRd,
			FUMemWrite        => FUMemWrite,
			-- to EX
			EXWriteData       => EXWriteData,
			-- to ID
			IDWriteData       => IDWriteData,
			IDMemWrite        => IDMemWrite
		);

	HDUnit : HazardDetectionUnit port map(
			clk            => clk,
	
			rst            => rst,
			run            => run,
			-- In
			IDEXmemread    => IDEXmemread,
			IDEXRegisterRt => IDEXRegisterRt,
			IFIDRegisterRt => IFIDRegisterRt,
			-- Out
			IFIDWrite      => IFIDWrite,
			PCWrite        => PCWrite
		);
	FORWARD_UNIT : ForwardingUnit port map(
			clk             => clk,
			rst             => rst,
			run             => run,
			-- IN
			IDEXRegisterRs  => IDEXRegisterRs,
			IDEXRegisterRt  => IDEXRegisterRt,
			EXMEMRegisterRd => EXMEMRegisterRd,
			MEMWBRegisterRd => MEMWBRegisterRd,

			-- Out
			MuxA            => FORWARD_MUXA,
			MuxB            => FORWARD_MUXB
		);

end arch_processor_core;
