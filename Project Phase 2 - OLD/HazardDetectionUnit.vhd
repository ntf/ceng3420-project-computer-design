library ieee;

use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.constants.all;
entity HazardDetectionUnit is
	port(
		clk                : in std_logic;
		rst                : in std_logic;
		run                : in std_logic;

		-- In
		signal IDEXmemread : in std_logic;
		IDEXRegisterRt : in std_logic_vector(4 downto 0);
		IFIDRegisterRt : in std_logic_vector(4 downto 0);
		-- Out
		signal IFIDWrite : out std_logic;
		signal PCWrite : out std_logic
		

	);
end HazardDetectionUnit;