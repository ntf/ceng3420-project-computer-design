library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use IEEE.numeric_std.all;
use work.constants.all;

entity alu is
	port(
		operation : in  std_logic_vector(3 downto 0);
		input_a   : in  std_logic_vector(31 downto 0);
		input_b   : in  std_logic_vector(31 downto 0);
		output    : out std_logic_vector(31 downto 0);
		zero      : out std_logic
	);
end alu;

architecture behavioral of alu is
	signal Result : std_logic_vector(31 downto 0);

begin
	-- result and zero output
	output <= Result;
	zero   <= '1' when (Result = x"00000000") else '0';

	ALUOutput : process(operation, input_a, input_b)
	begin
		case operation is
			-- and
			when ALU_AND =>
				Result <= input_a and input_b;
			-- or
			when ALU_OR =>
				Result <= input_a or input_b;
			-- add
			when ALU_ADD =>
				Result <= std_logic_vector(ieee.NUMERIC_STD.signed(input_a) + ieee.NUMERIC_STD.signed(input_b));
			-- sub 
			when ALU_SUB =>
				Result <= std_logic_vector(ieee.NUMERIC_STD.signed(input_a) - ieee.NUMERIC_STD.signed(input_b));

			-- set less than
			when ALU_SLT => if (ieee.NUMERIC_STD.signed(input_a) < ieee.NUMERIC_STD.signed(input_b)) then
					Result <= x"00000001";
				else
					Result <= x"00000000";
				end if;
			-- nor
			when ALU_NOR =>
				Result <= not (input_a or input_b);
			-- shift left
			when ALU_SLL =>
				Result <= std_logic_vector(ieee.NUMERIC_STD.unsigned(input_a) sll to_integer(ieee.NUMERIC_STD.signed(input_b)));

			-- set less than unsigned
			when ALU_SLTU => if (ieee.NUMERIC_STD.unsigned(input_a) < ieee.NUMERIC_STD.unsigned(input_b)) then
					Result <= x"00000001";
				else
					Result <= x"00000000";
				end if;
			-- LOAD UPPER IMMEDIATE
			when ALU_LUI =>
				Result            <= std_logic_vector(ieee.NUMERIC_STD.unsigned(input_b) sll 16);
			-- others
			when others => Result <= (others => '0');
		end case;
	end process ALUOutput;
end behavioral;