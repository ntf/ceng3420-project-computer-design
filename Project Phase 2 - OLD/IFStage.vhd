library ieee;

use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.constants.all;

-- fetch instruction from memory (need connect to memory in process_core)
-- PC update (may change due to Hazard detection unit)
-- PC update value
entity IFStage is
	port(
		clk             : in  std_logic;
		rst             : in  std_logic;
		run             : in  std_logic;
		-- indicate finish
		fin             : out std_logic;
		-- PC value when finish
		PCout           : out std_logic_vector(31 downto 0);

		-- memory related
		-- memory read address
		instaddr        : out std_logic_vector(31 downto 0);
		-- instruction 
		inst            : in  std_logic_vector(31 downto 0);
		-- signal to enable memory write		
		memwen          : out std_logic;
		-- memory  address
		memaddr         : out std_logic_vector(31 downto 0);
		-- memory write data
		memdw           : out std_logic_vector(31 downto 0);

		--nextAddr    : out std_logic_vector(31 downto 0);
		--instruction : out std_logic_vector(31 downto 0);

		--JumpAddr    : in  std_logic_vector(31 downto 0);
		--BranchAddr  : in  std_logic_vector(31 downto 0);

		--Branch      : in  std_logic;
		--Jump        : in  std_logic;  -- IF IN
		--Stall       : in  std_logic;
		--ALUZero     : in  std_logic;
		--ALUResult   : in  std_logic_vector(31 downto 0)

		--======================== Phase 2 code ========================--
		-- In
		Jump            : in  std_logic;
		JumpAddr        : in  std_logic_vector(31 downto 0);
		Branch          : in  std_logic;
		BranchAddr      : in  std_logic_vector(31 downto 0);
		PCWrite         : in  std_logic;

		-- Out to IF/ID
		IFIDinstruction : out std_logic_vector(31 downto 0);
		IFIDnextAddr    : out std_logic_vector(31 downto 0);

		-- Terminate
		ALUResult       : in  std_logic_vector(31 downto 0)
	);
end IFStage;

--======================== Phase 1 code ========================--
architecture arch_IFStage of IFStage is
	signal NextAddr : std_logic_vector(31 downto 0);

	signal PC                 : std_logic_vector(31 downto 0);
	signal IllegalInstruction : std_logic := '0';
	signal Terminated         : std_logic := '0';
begin

	-- Adder: PC + 4 => Next Addr
	NextAddr <= std_logic_vector(ieee.NUMERIC_STD.unsigned(PC) + 4);

	ProgramCounter : process(clk, rst)
		variable running : boolean := false;
		variable PCTemp  : std_logic_vector(31 downto 0);
		variable temp    : integer;
	begin
		if rst = '1' then
			PC         <= x"00004000";
			Terminated <= '0';
			running    := false;
			--		PCOut    <= PC;

			report "[DEBUG] RESET";
		elsif clk = '0' and run = '1' then
			PC         <= x"00004000";
			instaddr   <= x"00004000";
			Terminated <= '0';
			running    := true;
		end if;

		if running and clk = '1' and clk'event and rising_edge(clk) then
			if (Branch = '1') then
				--	report "[DEBUG] CLOCK RISING - Branch";
				PCTemp := BranchAddr;
			elsif (Jump = '1') then
				--	report "[DEBUG] CLOCK RISING - Jump";
				PCTemp := JumpAddr;
			else
				--	report "[DEBUG] CLOCK RISING - Next";
				PCTemp := NextAddr;
			end if;

			if Terminated = '1' or (inst = x"00000000") or (IllegalInstruction = '1') or (
				(inst(31 downto 26) = OP_LW or inst(31 downto 26) = OP_SW) and (
					not (conv_integer(ALUResult) mod 4 = 0)
				)) or (
				(Jump = '1') and not (conv_integer(JumpAddr) mod 4 = 0)
			) then
				running := false;
				fin     <= '1';
				PCOut   <= PC;
			end if;

			PC       <= PCTemp;
			instaddr <= PCTemp;
		end if;

	end process ProgramCounter;

end arch_IFStage;