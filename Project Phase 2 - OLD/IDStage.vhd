library ieee;

use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.constants.all;
entity IDStage is
	port(
		clk               : in  std_logic;
		rst               : in  std_logic;
		run               : in  std_logic;

		-- Data Input Signals
		IFIDnextAddr      : in  std_logic_vector(31 downto 0);
		IFIDinstruction   : in  std_logic_vector(31 downto 0);
		RegisterWriteAddr : in  std_logic_vector(4 downto 0)  := (others => '0');
		RegisterRegWrite  : in  std_logic;
		RegisterWriteData : in  std_logic_vector(31 downto 0) := (others => '0');
		--======================== Phase 1 code ========================--
		-- Control Output Signals
		RegDst            : out std_logic;
		Branch            : out std_logic;
		MemRead           : out std_logic;
		MemWrite          : out std_logic;
		ALUOp             : out std_logic_vector(3 downto 0);
		MemtoReg          : out std_logic;
		ALUSrc            : out std_logic;
		RegWrite          : out std_logic;
		Jump              : out std_logic;
		-- Register


		--======================== Phase 2 code ========================--

		-- Out 
		-- Control for WB
		WBMemtoReg        : out std_logic; --
		WBRegWrite        : out std_logic; --

		-- Control for M
		MBranch           : out std_logic;
		MMemRead          : out std_logic;
		MMemWrite         : out std_logic;

		-- Control for EX
		EXRegDst          : out std_logic; --
		EXALUOp           : out std_logic_vector(3 downto 0); --
		EXALUSrc          : out std_logic; --

		-- To IDEX buffer
		IDEXaddr          : out std_logic_vector(31 downto 0);
		IDEXfunct         : out std_logic_vector(5 downto 0);
		IDEXSignExtend    : out std_logic_vector(31 downto 0);
		IDEXRegisterRs    : out std_logic_vector(4 downto 0);
		IDEXRegisterRt    : out std_logic_vector(4 downto 0);
		IDEXRegisterRd    : out std_logic_vector(4 downto 0);
		IDEXRegOutputA    : out std_logic_vector(31 downto 0);
		IDEXRegOutputB    : out std_logic_vector(31 downto 0)
	);
end IDStage;
architecture arch_IDStage of IDStage is
	component sign_extend
		port(
			in_data  : in  std_logic_vector(15 downto 0);
			out_data : out std_logic_vector(31 downto 0)
		);
	end component;
	component control
		port(
			Opcode             : in  std_logic_vector(5 downto 0);
			Func               : in  std_logic_vector(5 downto 0);
			RegDst             : out std_logic;
			Jump               : out std_logic;
			Branch             : out std_logic;
			MemRead            : out std_logic;
			MemtoReg           : out std_logic;
			ALUOp              : out std_logic_vector(3 downto 0);
			MemWrite           : out std_logic;
			ALUSrc             : out std_logic;
			RegWrite           : out std_logic;
			IllegalInstruction : out std_logic
		);
	end component;

	signal IllegalInstruction : std_logic := '0';
begin
	Control32 : control port map(
			Opcode             => IFIDinstruction(31 downto 26),
			Func               => IFIDinstruction(5 downto 0),
			RegDst             => RegDst,
			Jump               => Jump,
			Branch             => Branch,
			MemRead            => MemRead,
			MemtoReg           => MemtoReg,
			ALUOp              => ALUOp,
			MemWrite           => MemWrite,
			ALUSrc             => ALUSrc,
			RegWrite           => RegWrite,
			-- if illegal instruction is enocuntered => 1 => Halt!
			IllegalInstruction => IllegalInstruction
		);
	SignExtender : sign_extend port map(
			in_data  => IFIDinstruction(15 downto 0),
			out_data => IDEXSignExtend
		);
end arch_IDStage;