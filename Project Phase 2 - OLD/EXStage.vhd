library ieee;

use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.constants.all;
entity EXStage is
	port(
		clk             : in  std_logic;
		rst             : in  std_logic;
		run             : in  std_logic;

		--======================== Phase 2 code ========================--
		-- IN
		-- From IDEX buffer
		IDEXaddr        : in  std_logic_vector(31 downto 0);
		IDEXfunct       : in  std_logic_vector(5 downto 0);
		IDEXRegOutputA  : in  std_logic_vector(31 downto 0);
		IDEXRegOutputB  : in  std_logic_vector(31 downto 0);
		IDEXSignExtend  : in  std_logic_vector(31 downto 0);
		IDEXRegisterRs  : in  std_logic_vector(4 downto 0);
		IDEXRegisterRt  : in  std_logic_vector(4 downto 0);
		IDEXRegisterRd  : in  std_logic_vector(4 downto 0);
		EXMEMALUresult  : in  std_logic_vector(31 downto 0);
		MEMWBResult     : in  std_logic_vector(31 downto 0);

		-- From Forwarding Unit
		MuxA            : in  std_logic;
		MuxB            : in  std_logic;
		-- Control for other stage
		-- Control for WB
		inWBMemtoReg    : in  std_logic;
		inWBRegWrite    : in  std_logic;

		-- Control for M
		inMBranch       : in  std_logic;
		inMMemRead      : in  std_logic;
		inMMemWrite     : in  std_logic;

		-- Control for EX
		inEXRegDst      : in  std_logic;
		inEXALUOp       : in  std_logic_vector(3 downto 0);
		inEXALUSrc      : in  std_logic;

		-- Out

		ALUZero         : out std_logic;

		-- Out to EXMEM
		-- Control out
		-- Control for WB
		outWBMemtoReg   : out std_logic;
		outWBRegWrite   : out std_logic;

		-- Control for M
		outMBranch      : out std_logic;
		outMMemRead     : out std_logic;
		outMMemWrite    : out std_logic;

		EXMEMALUResult  : out std_logic_vector(31 downto 0);
		EXMEMWriteData  : out std_logic_vector(31 downto 0);
		EXMEMRegisterRd : out std_logic_vector(4 downto 0)
	);
end EXStage;
architecture arch_EXStage of EXStage is
	component alu
		port(
			operation : in  std_logic_vector(3 downto 0);
			input_a   : in  std_logic_vector(31 downto 0);
			input_b   : in  std_logic_vector(31 downto 0);
			output    : out std_logic_vector(31 downto 0);
			zero      : out std_logic
		);
	end component;
-- ALU

begin
end arch_EXStage;