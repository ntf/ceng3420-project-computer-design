library ieee;

use ieee.numeric_std.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.constants.all;
entity WBStage is
	port(
		clk               : in  std_logic;
		rst               : in  std_logic;
		run               : in  std_logic;

		-- In
		-- Control WB
		inWBMemtoReg      : in  std_logic;
		inWBRegWrite      : in  std_logic;

		-- From MEMWB
		MEMWBMemData      : in  std_logic_vector(31 downto 0);
		MEMWBALUresult    : in  std_logic_vector(31 downto 0);
		MEMWBRegisterRd   : in  std_logic_vector(4 downto 0);

		-- Out
		-- to Forwarding Unit
		FUMEMWBRegisterRd : out std_logic_vector(4 downto 0);
		FUMemWrite        : out std_logic;
		-- to EX
		EXWriteData       : out std_logic_vector(31 downto 0);
		-- to ID
		IDWriteData       : out std_logic_vector(31 downto 0);
		IDMemWrite        : out std_logic
	);
end WBStage;